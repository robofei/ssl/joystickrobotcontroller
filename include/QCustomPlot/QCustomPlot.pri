

greaterThan(QT_MAJOR_VERSION, 4)


QT += \
    widgets \
    printsupport

HEADERS += \
    $$PWD/qcustomplot.h \
    $$PWD/QCustomPlot

SOURCES += \
    $$PWD/qcustomplot.cpp

INCLUDEPATH += \
    $$PWD
