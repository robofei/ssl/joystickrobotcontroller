/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */

#include "radiobase.h"

#include <QDataStream>
#include <QDebug>

RadioBase::RadioBase()
{
    serial = new QSerialPort(this);
    config = new Settings;
    monitored_robotid = 0;
    sensor_type = 0;

    portConnections();
}

bool RadioBase::configureSerialPort()
{
    serial->setPortName(config->name);
    serial->setBaudRate(config->baudRate);
    serial->setDataBits(config->dataBits);
    serial->setParity(config->parity);
    serial->setStopBits(config->stopBits);
    serial->setFlowControl(config->flowControl);

    return openPort();
}

bool RadioBase::openPort()
{
    return serial->open(QIODevice::ReadWrite);
}

void RadioBase::closePort()
{
    if (serial->isOpen())
        serial->close();
}

ErrorCode RadioBase::readData(QByteArray& retPack)
{
    retPack.append(serial->readAll());
    return this->decodePacket(retPack);
}

ErrorCode RadioBase::decodePacket(QByteArray& encodedPacket)
{
    const int byte_start = 6;
    int delim_pos = -1;
    int packet_size;
    int robotid;
    char checksum = 0;
    float a1, a2; // Variáveis utilizadas na conversão dos valores recebidos da
                  // odometria e sensor de corrente
    ErrorCode error_code = ErrorCode::NOPCKT_ERROR;

    const int INTERM_MAX_SIZE = 256;
    const int SAFETY_THRESHOLD = 50;

    int safety_counter = 0;

    while (encodedPacket.size() > 4)
    {                     // there's enough data to mean something
        safety_counter++; // The safety check is a measure to prevent the
                          // routine from getting stuck - it should never occur
                          // under normal conditions

        if (safety_counter > SAFETY_THRESHOLD)
        { // if safety counter reaches the threshold
            encodedPacket.clear();
            return SAFETY_THRESHOLD_ERROR;
        }

        if (encodedPacket.size() > INTERM_MAX_SIZE)
        { // if buffer became too big, delete older packets -
          // temporary - to be re-evaluated
            encodedPacket.remove(
                0, encodedPacket.size() -
                       INTERM_MAX_SIZE); // remove older packets .... temporary
        }

        error_code = DelimiterFinder(encodedPacket, 0, delim_pos);

        if (error_code !=
            ErrorCode::NOPCKT_ERROR) // Checando se houve algum erro
                                     // ao procurar o delimitador
        {
            return error_code;
        }

        if (delim_pos > 0)
            encodedPacket.remove(
                0,
                delim_pos); // discards garbage before the delimiter

        // packet_size is the size given by Length bytes + 1 byte delimiter + 2
        // bytes lenght + 1 byte checksum = Length + 4
        packet_size = (int)(encodedPacket[1] * 256 + encodedPacket[2]) + 4;

        if (encodedPacket.size() <
            delim_pos + PREAMBLE_SIZE) // Pacote incompleto
        {
            error_code = ErrorCode::PACKET_INCOMPLETE;
            return error_code;
        }
        else if (encodedPacket.size() < (packet_size)) // the packet isn't
                                                       // complete
            return ErrorCode::PACKET_INCOMPLETE;

        if ((unsigned char)encodedPacket[3] != ROBOFEI_IDENTIFIER)
        {
            encodedPacket.clear(); // erase data, unhandled type of packet
            return ErrorCode::OTHER_RADIO_PCKT;
        }
        // Aparentemente o checksum nao esta funcionando, por isso esta parte
        // está comentada

        //        for(int n = delim_pos + 3; n < packet_size-1; n++)//calcula o
        //        valor do checksum para o pacote recebido { //'delim_pos+3' é a
        //        posição do primeiro valor significativo do pacote (0x90)
        //            checksum += encodedPacket[n];
        //        }
        //        checksum = ~checksum;

        //        if(checksum != encodedPacket[packet_size-1])//'packet_size-1'
        //        é a posição onde está o valor do checksum
        //        {
        //            error_code = ErrorCode::CHECKSUM_ERROR;
        //            return error_code;
        //        }

        if ((unsigned char)encodedPacket[byte_start] == PKT_BEGIN &&
            (unsigned char)encodedPacket[packet_size - 2] == PKT_END)
        {
            robotid = encodedPacket[byte_start + 2];
            if (robotid < PLAYERS_PER_SIDE && robotid >= 0)
            {

                // calculation of the battery level, according to the hardware
                // of the main board
                battery_level[robotid] = (float)encodedPacket[byte_start + 3] *
                                         5.0f / 4096.f * 11.f * 11.f;

                kick_sensor[robotid] = (int)encodedPacket[byte_start + 1];

                if (robotid == monitored_robotid)
                {
                    if ((unsigned char)encodedPacket[byte_start + 4] ==
                        sensor_type)
                    { // type of sensor data received. 0=odometry,
                      // 1=current
                        if (sensor_type == 0)
                        {
                            a1 = 0;
                            a2 = 1.0f; // offset and scale multiplier for
                                       // odometry - basically there's no
                                       // conversion
                        }
                        else
                        {
                            a1 = -37.5f;
                            a2 = 0.01831f;
                        } // offset and scale multiplier for the current sensor
                          // - values according to IC datasheet

                        if (packet_size > byte_start + 20)
                        { // if packet is big enough to have full sensor data

                            // Atribuição do valor da odometria de cada motor
                            measured_1.append(
                                convertOdometry(encodedPacket[byte_start + 5],
                                                encodedPacket[byte_start + 6]));
                            measured_2.append(
                                convertOdometry(encodedPacket[byte_start + 7],
                                                encodedPacket[byte_start + 8]));
                            measured_3.append(convertOdometry(
                                encodedPacket[byte_start + 9],
                                encodedPacket[byte_start + 10]));
                            measured_4.append(convertOdometry(
                                encodedPacket[byte_start + 11],
                                encodedPacket[byte_start + 12]));

                            encoderCount[0] =
                                convertOdometry(encodedPacket[byte_start + 13],
                                                encodedPacket[byte_start + 14]);
                            encoderCount[1] =
                                convertOdometry(encodedPacket[byte_start + 15],
                                                encodedPacket[byte_start + 16]);
                            encoderCount[2] =
                                convertOdometry(encodedPacket[byte_start + 17],
                                                encodedPacket[byte_start + 18]);
                            encoderCount[3] =
                                convertOdometry(encodedPacket[byte_start + 19],
                                                encodedPacket[byte_start + 20]);

                            qDebug()
                                << "Robot " << robotid << ") Vcap = "
                                << QString::number(
                                       (quint8)encodedPacket[byte_start + 21])
                                << " V";
                            qDebug()
                                << "Robot " << robotid << ") PacketF = "
                                << QString::number(
                                       (quint8)encodedPacket[byte_start + 22])
                                << " Hz";
                        }
                    }
                }

                // checks if there are lost packets on the transmission
                rx_counter[robotid][0] = (int)encodedPacket[packet_size - 3];
                if ((rx_counter[robotid][0] - rx_counter[robotid][1] > 1) &&
                    !(rx_counter[robotid][0] == 0 &&
                      rx_counter[robotid][1] == 255))
                {
                    int tmp1 = rx_counter[robotid][0] - rx_counter[robotid][1];
                    if (tmp1 < 0)
                        tmp1 += 255;
                    rx_lost[robotid] += tmp1;
                }
                rx_counter[robotid][1] = rx_counter[robotid][0];
            }
        }
        encodedPacket.clear(); // delete the packet. Always needed after a
                               // packet has been processed
    }
    return error_code;
}

ErrorCode RadioBase::DelimiterFinder(QByteArray& packet, int bytestart,
                                     int& delimiter_pos)
{

    for (int n = bytestart; n < packet.size(); n++)
    {
        if ((unsigned char)packet[n] == PKT_DELIMITER)
        {
            delimiter_pos = n;
            return ErrorCode::NOPCKT_ERROR;
        }
    }

    return ErrorCode::DELIMITER_NOT_FOUND;
}

bool RadioBase::sendData(QByteArray& encodedPacket)
{
    if (!serial->isOpen())
        return false;

    QDataStream stream(serial);
    stream << encodedPacket;

    return true;
}

void RadioBase::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError)
        closePort();
}

void RadioBase::portConnections()
{
    connect(serial,
            static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(
                &QSerialPort::error),
            this, &RadioBase::handleError);
}

void RadioBase::setConfig(Settings* pconfig)
{
    memcpy(this->config, pconfig, sizeof(Settings));
}

QByteArray RadioBase::encodePacket(int destAdd, QByteArray& packet)
{
    unsigned char checksum = 0;
    int packetsize = PREAMBLE_SIZE + packet.size() + 1;
    int packetlength = packetsize - 4;
    QByteArray encodedPacket = QByteArray(packetsize, '\0');

    // Preamble - Nordic RoboFei protocol v1.0
    encodedPacket[0] = PKT_DELIMITER;                     // delimiter
    encodedPacket[1] = (unsigned char)packetlength / 256; // Length MSB
    encodedPacket[2] = (unsigned char)packetlength;       // Length LSB
    encodedPacket[3] = PKT_TX_REQUEST;                    // TX_request
    encodedPacket[4] = PKT_FRAME_ID;                      // Frame ID (byte 5)
    encodedPacket[5] = (unsigned char)destAdd;            // Destination Address

    for (int n = 0; n < (int)packet.size(); n++) // counts packet size
    {
        encodedPacket[PREAMBLE_SIZE + n] = packet[n];
    }

    // checksum calculation
    for (int n = 3; n < encodedPacket.length(); n++)
    {
        checksum += encodedPacket[n];
    }

    checksum = ~checksum;
    encodedPacket[packetsize - 1] = checksum;

    return encodedPacket;
}

QByteArray RadioBase::RadioPacketize(Robot* robot)
{
    QByteArray data_pack = QByteArray(PKT_SIZE, '\0');

    data_pack[PKT_BEGIN_P] = PKT_BEGIN;
    data_pack[PKT_END_P] = PKT_END;
    data_pack[PKT_COUNTER_P] = robot->radiotx_counter++;
    data_pack[FEEDBACK_P] = PKT_REQUEST_FEEDBACK; // sync_radio

    data_pack[ROLLER_V_P] = (unsigned char)robot->cmd_rollerspd;
    data_pack[ID_P] = (unsigned char)robot->id;
    data_pack[VX_P] = (unsigned char)robot->cmd_vx;
    data_pack[VY_P] = (unsigned char)robot->cmd_vy;
    data_pack[VW_P] = (unsigned char)robot->cmd_w;

    qint8 kickByte = 0;
    const quint8 minKick = 0, maxKick = 100;

    if (robot->kick == KICK_NONE)
    {
        kickByte = 0;
    }
    else
    {
        if (robot->kick == KICK_CUSTOM)
            kickByte |= DIRECT_KICK_bit;
        else if (robot->kick == CHIP_KICK_CUSTOM)
            kickByte |= CHIP_KICK_bit;

        kickByte = kickByte |
                   (KICK_STRENGTH_MASK &
                    qMax(qMin((quint8)robot->kickStrength, maxKick), minKick));
    }
    data_pack[KICK_CFG_P] = kickByte;

    data_pack[THETA_LSB_P] = robot->theta & POSE_LOW_MASK;         // Theta LOW
    data_pack[THETA_MSB_P] = (robot->theta & POSE_HIGH_MASK) >> 8; // Theta HIGH
    data_pack[X_LSB_P] = robot->x & POSE_LOW_MASK;                 // X LOW
    data_pack[X_MSB_P] = (robot->x & POSE_HIGH_MASK) >> 8;         // X HIGH
    data_pack[Y_LSB_P] = robot->y & POSE_LOW_MASK;                 // Y LOW
    data_pack[Y_MSB_P] = (robot->y & POSE_HIGH_MASK) >> 8;         // Y HIGH

    return data_pack;
}

void RadioBase::getMeasures(QVector<int>& m1, QVector<int>& m2,
                            QVector<int>& m3, QVector<int>& m4)
{
    m1 = measured_1;
    m2 = measured_2;
    m3 = measured_3;
    m4 = measured_4;
}

int RadioBase::convertOdometry(unsigned char _high, unsigned char _low)
{
    int result = 6000;
    if (((_high & 0x10) >> 4) == false) // positivo
    {
        result = (((_high & 0x0F) << 8 | _low));
    }
    else
    {
        result = (-((_high & 0x0F) << 8 | _low));
        // qDebug() << "Result is" << result << " High is" << _high << " Low is"
        // << _low;
    }
    return result;
}
