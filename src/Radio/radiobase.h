/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */

#include <QSerialPort>
#include <QVector>

#include "Robot/robot.h"

#ifndef RADIOBASE_H
#define RADIOBASE_H

/**
 * @brief
 *
 */
typedef struct // Estrutura com os parâmetros da porta serial
{
    QString name;                         /**< TODO: describe */
    QSerialPort::DataBits dataBits;       /**< TODO: describe */
    QSerialPort::FlowControl flowControl; /**< TODO: describe */
    QSerialPort::Parity parity;           /**< TODO: describe */
    QSerialPort::StopBits stopBits;       /**< TODO: describe */
    int baudRate;                         /**< TODO: describe */
    bool localEchoEnabled;                /**< TODO: describe */
                                          /**
                                           * @brief
                                           *
                                           */
} Settings;

/**
 * @brief
 *
 */
enum ErrorCode
{
    CHECKSUM_ERROR = 0x0A,
    DELIMITER_NOT_FOUND = 0x0B,
    NOPCKT_ERROR = 0x0C,
    PACKET_INCOMPLETE = 0x0D,
    OTHER_RADIO_PCKT = 0x0E,
    SAFETY_THRESHOLD_ERROR = 0x0F,
};

typedef enum
{
    ID_P = 1,          // Posiçãoo do ID do robô no pacote
    VX_P = 2,          // Posiçãoo da velocidade em X no pacote
    VY_P = 3,          // Posiçãoo da velocidade em Y no pacote
    VW_P = 4,          // Posiçãoo da velocidade de rotação no pacote
    PKT_COUNTER_P = 7, // Posição do byte de contagem de pacotes
    FEEDBACK_P = 8,    // Posiçãoo do comando de enviar feedback no pacote
    KICK_CFG_P = 9,    // Posiçãoo da força do chute no pacote
    ROLLER_V_P = 10,   // Posiçãoo da velocidade do roller no pacote
    X_LSB_P = 11,      // Posição da parte baixa da posição X
    X_MSB_P = 12,      // Posição da parte alta da posição X
    Y_LSB_P = 13,      // Posição da parte baixa da posição Y
    Y_MSB_P = 14,      // Posição da parte alta da posição Y
    THETA_LSB_P = 5,   // Posição da parte baixa da posição Theta
    THETA_MSB_P = 6,   // Posição da parte alta da posição Theta
    PKT_BEGIN_P = 0,
    PKT_END_P = 15
} RX_PACKET_PROTOCOL;

typedef enum
{
    PKT_SIZE = 16,
    PREAMBLE_SIZE = 6,
    PKT_BEGIN = 0x62,
    PKT_END = 0x65,
    PKT_DELIMITER = 0x7E,
    PKT_TX_REQUEST = 0x10,
    PKT_FRAME_ID = 0x00,
    PKT_REQUEST_FEEDBACK = 0x80,
    ROBOFEI_IDENTIFIER = 0x90
} PACKET_CONTROL_BYTES;

typedef enum
{
    KICK_TYPE_MASK = 0x80,
    DIRECT_KICK_bit = 0x80,
    CHIP_KICK_bit = 0x00,
    KICK_STRENGTH_MASK = 0x7F,
    POSE_HIGH_MASK = 0xFF00,
    POSE_LOW_MASK = 0x00FF,
} PACKET_MASKS;

/**
 * @brief
 *
 */
class RadioBase : public QObject
{
    Q_OBJECT

private:
    /**
     * @brief
     *
     * @param error
     */
    void handleError(QSerialPort::SerialPortError error);
    /**
     * @brief
     *
     * @return bool
     */
    bool openPort(); // Abre a porta serial

    /**
     * @brief
     *
     * @param packet
     * @param bytestart
     * @param delimiter_pos
     * @return ErrorCode
     */
    ErrorCode DelimiterFinder(QByteArray& packet, int bytestart,
                              int& delimiter_pos);

    Settings* config; /**< TODO: describe */

public:
    // Métodos utilizados no envio de um pacote
    /**
     * @brief
     *
     * @param encodedPacket
     * @return bool
     */
    bool sendData(QByteArray& encodedPacket);
    /**
     * @brief
     *
     * @param destAdd
     * @param data
     * @return QByteArray
     */
    QByteArray encodePacket(int destAdd, QByteArray& data);
    /**
     * @brief
     *
     * @param robot
     * @return QByteArray
     */
    QByteArray RadioPacketize(Robot* robot);

    // Métodos utilizados no recebimento de um pacote
    /**
     * @brief
     *
     * @param retPack
     * @return ErrorCode
     */
    ErrorCode readData(QByteArray& retPack);
    /**
     * @brief
     *
     * @param encodedPacket
     * @return ErrorCode
     */
    ErrorCode decodePacket(
        QByteArray& encodedPacket); // Seria melhor utilizar outro nome
                                    // para o argumento? ver sendData

    // Métodos utilizados na configuração da porta serial
    /**
     * @brief
     *
     */
    void portConnections();
    /**
     * @brief
     *
     */
    void closePort();
    /**
     * @brief
     *
     * @param pconfig
     */
    void setConfig(Settings* pconfig);

    QSerialPort* serial; /**< TODO: describe */

    /**
     * @brief
     *
     * @param m1
     * @param m2
     * @param m3
     * @param m4
     */
    void getMeasures(QVector<int>& m1, QVector<int>& m2, QVector<int>& m3,
                     QVector<int>& m4);

    int convertOdometry(unsigned char _high, unsigned char _low);

    // Variáveis utilizadas na decodificação dos pacotes recebidos
    QVector<int> measured_1, measured_2, measured_3,
        measured_4; /**< TODO: describe */
    int encoderCount[4];

    float battery_level[PLAYERS_PER_SIDE];       /**< TODO: describe */
    unsigned char kick_sensor[PLAYERS_PER_SIDE]; /**< TODO: describe */
    int rx_counter[PLAYERS_PER_SIDE][2];         /**< TODO: describe */
    int rx_lost[PLAYERS_PER_SIDE];               /**< TODO: describe */
    int monitored_robotid;                       /**< TODO: describe */
    int sensor_type;                             /**< TODO: describe */

    /**
     * @brief
     *
     */
    RadioBase(); // Construtor da classe

public slots:
    /**
     * @brief
     *
     * @return bool
     */
    bool configureSerialPort(); // Configura a porta serial
};
#endif // RADIOBASE_H
