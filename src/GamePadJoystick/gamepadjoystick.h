/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#ifndef GAMEPADJOYSTICK_H
#define GAMEPADJOYSTICK_H

#include <QObject>
#include <QGamepad>
#include <QGamepadManager>

///
/// \brief Struct contendo as informações de um evento de um axis
///
struct GamePadJoystickAxisEvent{

    QGamepadManager::GamepadAxis axis{QGamepadManager::AxisInvalid}; /**< Which axis */
    qint8 value{0}; /**< Valor do axis */

    ///
    /// \brief Construtor padrão
    /// \details * Atribui QGamepadManager::AxisInvalid ao axis
    /// * Atribui 0 ao value
    ///
    GamePadJoystickAxisEvent(){

    };

    ///
    /// \brief GamePadJoystickAxisEvent
    /// \param a - Which axis
    /// \param v - Valor do axis
    ///
    GamePadJoystickAxisEvent(QGamepadManager::GamepadAxis a,
                             qint8 v){
        axis = a;
        value = v;
    }
};

///
/// \brief Struct contendo as informações de um evento de um button
///
struct GamePadJoystickButtonEvent{

    QGamepadManager::GamepadButton button{QGamepadManager::ButtonInvalid}; /**< Which button */
    bool pressed{false}; /**< Estado do button */

    ///
    /// \brief Construtor padrão
    /// \details * Atribui QGamepadManager::ButtonInvalid ao button
    /// * Atribui false (realesed) ao pressed
    ///
    GamePadJoystickButtonEvent(){

    };

    ///
    /// \brief Construtor padrão
    /// \param b - Which buton
    /// \param v - Estadi do button
    ///
    GamePadJoystickButtonEvent(QGamepadManager::GamepadButton b,
                               bool& p){
        button = b;
        pressed = p;
    }

};

///
/// \brief Struct contendo as informações de um evento de um buttonWithValue
///
struct GamePadJoystickButtonWithValueEvent{

    QGamepadManager::GamepadButton button{QGamepadManager::ButtonInvalid}; /**< Which button */
    quint8 value{0};/**< valor do button */

    ///
    /// \brief Construtor padrão
    /// \details * Atribui QGamepadManager::ButtonInvalid ao button
    /// * Atribui 0 ao value
    ///
    GamePadJoystickButtonWithValueEvent(){

    };

    ///
    /// \brief Construtor padrão
    /// \param b - Which buton
    /// \param v - Valor do button
    ///
    GamePadJoystickButtonWithValueEvent(QGamepadManager::GamepadButton b,
                                        quint8 v){
        button = b;
        value = v;
    };

};

///
/// \brief A classe GamePadJoystick é responsável por transmitir os sinais vindos do gamepad
/// \details Esta classe serve como um meio termo entre a classe InterfaceJoystick e os eventos do Gamepad
/// (axis, button ou button with value).
///
class GamePadJoystick : public QObject
{
    Q_OBJECT
public:

    ///
    /// \brief Construtor padrão da classe GamePadJoystick
    ///
    explicit GamePadJoystick(QObject *parent = nullptr);

    ///
    /// \brief Destruidor padrão da classe GamePadJoystick
    ///
    ~GamePadJoystick();

    ///
    /// \brief Procura gamepads conectados no sistema
    /// \details Esta função procura gamepads conectados no sistema e configura
    /// o membro da classe gamePad com o primeiro dispositivo encontrado.
    /// * Retorna true se achou algum gamepad;
    /// * Retorna false se não encontrou nenhum gamepad.
    ///
    bool bGamePadConnected();


    bool bIsEnable() const;

    QString sGetName()const ;
signals:

    ///
    /// \brief Emite evento do tipo GamePadJoystickAxisEvent
    /// \param event - Evento contendo o which do axis e seu valor
    ///
    void SIGNAL_axisEvent(const GamePadJoystickAxisEvent event);

    ///
    /// \brief Emite evento do tipo GamePadJoystickButtonEvent
    /// \param event - Evento contendo o which do button e seu valor
    ///
    void SIGNAL_buttonEvent(const GamePadJoystickButtonEvent event);

    ///
    /// \brief Emite evento do tipo GamePadJoystickButtonWithValueEvent
    /// \param event - Evento contendo o which do axis e seu valor
    ///
    void SIGNAL_buttonWithValueEvent(const GamePadJoystickButtonWithValueEvent event);

public slots:

    ///
    /// \brief Ativa o gamepad
    /// \details Quando essa função é chamada, todos os eventos enviados pelo gamePad
    /// devem ser tratados nessa classe.
    ///
    void SLOT_enableGamePadJoystick();

    ///
    /// \brief Desativa o gamepad
    /// \details Desconecta todos os sinais do gamePad
    ///
    void SLOT_disableGamePadJoystick();

private slots:

    ///
    /// \brief Recebe o valor do axis LX e emite um sinal GamePadJoystickAxisEvent referente ao evento
    /// \param value - valor do axis em uma escala de -1 a 1
    ///
    void SLOT_getAxisLX(double value);

    ///
    /// \brief Recebe o valor do axis LY e emite um sinal GamePadJoystickAxisEvent referente ao evento
    /// \param value - valor do axis em uma escala de -1 a 1
    ///
    void SLOT_getAxisLY(double value);

    ///
    /// \brief Recebe o valor do axis RX e emite um sinal GamePadJoystickAxisEvent referente ao evento
    /// \param value - valor do axis em uma escala de -1 a 1
    ///
    void SLOT_getAxisRX(double value);

    ///
    /// \brief Recebe o valor do axis RY e emite um sinal GamePadJoystickAxisEvent referente ao evento
    /// \param value - valor do axis em uma escala de -1 a 1
    ///
    void SLOT_getAxisRY(double value);



    ///
    /// \brief Recebe o valor do button L2 e emite um sinal GamePadJoystickButtonWithValueEvent referente ao evento
    /// \param value - valor do button em uma escala de 0 a 1
    ///
    void SLOT_getButtonWithValueL2(double value);

    ///
    /// \brief Recebe o valor do button R2 e emite um sinal GamePadJoystickButtonWithValueEvent referente ao evento
    /// \param value - valor do button em uma escala de 0 a 1
    ///
    void SLOT_getButtonWithValueR2(double value);



    ///
    /// \brief Recebe o valor do button A e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonA(bool pressed);

    ///
    /// \brief Recebe o valor do button B e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonB(bool pressed);

    ///
    /// \brief Recebe o valor do button X e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonX(bool pressed);

    ///
    /// \brief Recebe o valor do button Y e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonY(bool pressed);


    ///
    /// \brief Recebe o valor do button L1 e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonL1(bool pressed);

    ///
    /// \brief Recebe o valor do button L3 e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonL3(bool pressed);


    ///
    /// \brief Recebe o valor do button Select e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonSelect(bool pressed);

    ///
    /// \brief Recebe o valor do button Center e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonCenter(bool pressed);

    ///
    /// \brief Recebe o valor do button Start e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonStart(bool pressed);


    ///
    /// \brief Recebe o valor do button Guide e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonGuide(bool pressed);


    ///
    /// \brief Recebe o valor do button R1 e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonR1(bool pressed);

    ///
    /// \brief Recebe o valor do button R3 e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonR3(bool pressed);


    ///
    /// \brief Recebe o valor do button Down e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonDown(bool pressed);

    ///
    /// \brief Recebe o valor do button Left e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonLeft(bool pressed);

    ///
    /// \brief Recebe o valor do button Right e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonRight(bool pressed);

    ///
    /// \brief Recebe o valor do button Up e emite um sinal GamePadJoystickButtonEvent referente ao evento
    /// \param pressed - estado do button
    ///
    void SLOT_getButtonUp(bool pressed);

private:
    QScopedPointer<QGamepad> gamepad;

    bool bEnable{false};
};

#endif // GAMEPADJOYSTICK_H
