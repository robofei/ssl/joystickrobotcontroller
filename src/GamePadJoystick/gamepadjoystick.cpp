/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#include "gamepadjoystick.h"

#include <QDebug>

GamePadJoystick::GamePadJoystick(QObject *parent) : QObject(parent)
{
    gamepad.reset();
}

GamePadJoystick::~GamePadJoystick()
{

}

bool GamePadJoystick::bGamePadConnected()
{

    const QGamepadManager* manager = QGamepadManager::instance(); // point to instance

    const QList<int>& gamepads = manager->connectedGamepads();

    if (gamepads.isEmpty()) {
        qWarning("Did not find any connected gamepads");

        gamepad.reset();

        return false;
    }

    // else
    for (auto n : gamepads)if(manager->isGamepadConnected(n)){
        qDebug()<< QString::number(n)
                << "   "
                << manager->gamepadName(n)
                << " conectado";
    }

    gamepad.reset( new QGamepad(gamepads.first(), this) );

    return true;

}

bool GamePadJoystick::bIsEnable() const
{
    return bEnable;
}

QString GamePadJoystick::sGetName() const
{
    return QString(gamepad->name() + "    " + QString::number(gamepad->deviceId()));
}

void GamePadJoystick::SLOT_enableGamePadJoystick()
{
    if(!gamepad)
        return;

    connect(gamepad.get(), &QGamepad::axisLeftXChanged,
            this, &GamePadJoystick::SLOT_getAxisLX);

    connect(gamepad.get(), &QGamepad::axisLeftYChanged,
            this, &GamePadJoystick::SLOT_getAxisLY);

    connect(gamepad.get(), &QGamepad::axisRightXChanged,
            this, &GamePadJoystick::SLOT_getAxisRX);

    connect(gamepad.get(), &QGamepad::axisRightYChanged,
            this, &GamePadJoystick::SLOT_getAxisRY);


    connect(gamepad.get(), &QGamepad::buttonL2Changed,
            this, &GamePadJoystick::SLOT_getButtonWithValueL2);

    connect(gamepad.get(), &QGamepad::buttonR2Changed,
            this, &GamePadJoystick::SLOT_getButtonWithValueR2);


    connect(gamepad.get(), &QGamepad::buttonAChanged,
            this, &GamePadJoystick::SLOT_getButtonA);

    connect(gamepad.get(), &QGamepad::buttonBChanged,
            this, &GamePadJoystick::SLOT_getButtonB);

    connect(gamepad.get(), &QGamepad::buttonXChanged,
            this, &GamePadJoystick::SLOT_getButtonX);

    connect(gamepad.get(), &QGamepad::buttonYChanged,
            this, &GamePadJoystick::SLOT_getButtonY);



    connect(gamepad.get(), &QGamepad::buttonL1Changed,
            this, &GamePadJoystick::SLOT_getButtonL1);

    connect(gamepad.get(), &QGamepad::buttonL3Changed,
            this, &GamePadJoystick::SLOT_getButtonL3);


    connect(gamepad.get(), &QGamepad::buttonSelectChanged,
            this, &GamePadJoystick::SLOT_getButtonSelect);

    connect(gamepad.get(), &QGamepad::buttonCenterChanged,
            this, &GamePadJoystick::SLOT_getButtonCenter);

    connect(gamepad.get(), &QGamepad::buttonStartChanged,
            this, &GamePadJoystick::SLOT_getButtonStart);


    connect(gamepad.get(), &QGamepad::buttonGuideChanged,
            this, &GamePadJoystick::SLOT_getButtonGuide);


    connect(gamepad.get(), &QGamepad::buttonR1Changed,
            this, &GamePadJoystick::SLOT_getButtonR1);

    connect(gamepad.get(), &QGamepad::buttonR3Changed,
            this, &GamePadJoystick::SLOT_getButtonR3);


    connect(gamepad.get(), &QGamepad::buttonDownChanged,
            this, &GamePadJoystick::SLOT_getButtonDown);

    connect(gamepad.get(), &QGamepad::buttonLeftChanged,
            this, &GamePadJoystick::SLOT_getButtonLeft);

    connect(gamepad.get(), &QGamepad::buttonRightChanged,
            this, &GamePadJoystick::SLOT_getButtonRight);

    connect(gamepad.get(), &QGamepad::buttonUpChanged,
            this, &GamePadJoystick::SLOT_getButtonUp);


    bEnable = true;
}

void GamePadJoystick::SLOT_disableGamePadJoystick()
{
    gamepad->disconnect();
    bEnable = false;
}

void GamePadJoystick::SLOT_getAxisLX(double value)
{
    emit SIGNAL_axisEvent(GamePadJoystickAxisEvent(QGamepadManager::AxisLeftX,
                                                   static_cast<qint8>(100*value)));
}

void GamePadJoystick::SLOT_getAxisLY(double value)
{
    emit SIGNAL_axisEvent(GamePadJoystickAxisEvent(QGamepadManager::AxisLeftY,
                                                   static_cast<qint8>(100*value)));

}

void GamePadJoystick::SLOT_getAxisRX(double value)
{
    emit SIGNAL_axisEvent(GamePadJoystickAxisEvent(QGamepadManager::AxisRightX,
                                                   static_cast<qint8>(100*value)));

}

void GamePadJoystick::SLOT_getAxisRY(double value)
{
    emit SIGNAL_axisEvent(GamePadJoystickAxisEvent(QGamepadManager::AxisRightY,
                                                   static_cast<qint8>(100*value)));

}

void GamePadJoystick::SLOT_getButtonWithValueL2(double value)
{
    emit SIGNAL_buttonWithValueEvent(GamePadJoystickButtonWithValueEvent(QGamepadManager::ButtonL2,
                                                                         static_cast<quint8>(100*value)));

}

void GamePadJoystick::SLOT_getButtonWithValueR2(double value)
{
    emit SIGNAL_buttonWithValueEvent(GamePadJoystickButtonWithValueEvent(QGamepadManager::ButtonR2,
                                                                         static_cast<quint8>(100*value)));

}

void GamePadJoystick::SLOT_getButtonA(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonA,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonB(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonB,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonX(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonX,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonY(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonY,
                                                       pressed));

}



void GamePadJoystick::SLOT_getButtonL1(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonL1,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonL3(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonL3,
                                                       pressed));

}




void GamePadJoystick::SLOT_getButtonSelect(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonSelect,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonCenter(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonCenter,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonStart(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonStart,
                                                       pressed));

}



void GamePadJoystick::SLOT_getButtonGuide(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonGuide,
                                                       pressed));

}



void GamePadJoystick::SLOT_getButtonR1(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonR1,
                                                       pressed));

}

void GamePadJoystick::SLOT_getButtonR3(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonR3,
                                                       pressed));

}





void GamePadJoystick::SLOT_getButtonDown(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonDown,
                                                        pressed));

}



void GamePadJoystick::SLOT_getButtonLeft(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonLeft,
                                                       pressed));

}


void GamePadJoystick::SLOT_getButtonRight(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonRight,
                                                       pressed));

}


void GamePadJoystick::SLOT_getButtonUp(bool  pressed)
{
    emit SIGNAL_buttonEvent(GamePadJoystickButtonEvent(QGamepadManager::ButtonUp,
                                                       pressed));

}


