/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#ifndef ROBOT_H
#define ROBOT_H

const long PLAYERS_PER_SIDE = 8; /**< TODO: describe */


/**
 * @brief 
 *
 */
enum KICKTYPE
{
    KICK_NONE         = 0x00,
    KICK_CUSTOM       = 0x01, // kick
    CHIP_KICK_CUSTOM  = 0x02, // chip
    ANGLE_KICK_CUSTOM = 0x03, // OBS: Estes valores provavelmente mudarão
};                            // quando o chute direcional for implementado de
                              // fato

/**
 * @brief 
 *
 */
enum ROBOTHARDWARE : signed char
{
    v2010b,
    v2013
};

/**
 * @brief 
 *
 */
typedef struct
{
    long left, right, top, bottom; /**< TODO: describe */
/**
 * @brief 
 *
 */
} Bounds;

/**
 * @brief 
 *
 */
typedef struct
{

    unsigned char id;/// The id and radio address of the robot /**< TODO: describe */
    ROBOTHARDWARE hw_version; /**< TODO: describe */

    float rotation; ///< robot angle in rad /**< TODO: describe */
    bool kickSensor; ///< flag that contains robot ball sensor /**< TODO: describe */
    float battery; /**< TODO: describe */

    unsigned char radiotx_counter; /**< TODO: describe */

    // robot actuators
    float cmd_r, cmd_rho, cmd_theta, cmd_avgspd; //commands to actuators of the robot in polar coordinates /**< TODO: describe */
    float cmd_vx, cmd_vy, cmd_w;//commands to actuators of the robot in polar coordinates /**< TODO: describe */
    int cmd_rollerspd;    ///command roller velocity /**< TODO: describe */

    KICKTYPE kick; ///< strength of the kick. 2(weak), 4(medium), 8(strong) /**< TODO: describe */
    bool roller; ///< activates the roller/dribler /**< TODO: describe */
    float customKick; ///< stores a custom value for the kick /**< TODO: describe */
    float kickStrength;/// intensity/energy of the kick or chip /**< TODO: describe */

    qint16 x;
    qint16 y;
    qint8 theta;
/**
 * @brief 
 *
 */
} Robot;

#endif // ROBOT_H
