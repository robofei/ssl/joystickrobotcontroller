/*
 * Copyright (c) 2015-2017  Alex Spataru <alex_spataru@outlook.com>
 * Copyright (C) 2021-2022  Bruno Bollos Correa
 * Copyright (C) 2021-2022  RoboFEI / Small Size League
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef VIRTUALJOYSTICK_H
#define VIRTUALJOYSTICK_H

#include <QKeyEvent>



/**
 * @brief Struct com as informações de um evento de axis
 *
 */
struct VirtualJoystickButtonEvent{

    Qt::Key key{Qt::Key_unknown}; /**< Key da tecla */
    bool pressed{false}; /**< Estado da tecla (true para pressionado, false para soltado) */

    /**
     * @brief Construtor Padrão
     *
     * @param k - Key da tecla
     * @param p - Estado da tecla
     */
    VirtualJoystickButtonEvent( Qt::Key k,
                                 bool p){

        key = k;
        pressed = p;
    }

    /**
     * @brief Construtor Padrão
     * @details * key recebe Qt::Key_unknown
     * * pressed recebe false (realesed)
     *
     */
    VirtualJoystickButtonEvent(){

        // key = Qt::Key_unknown;
        // pressed = false;
    }

};


/**
 * @brief Struct com as informações de um evento de um button
 *
 */
struct VirtualJoystickAxisEvent {
    quint8 axis{255}; /**< Which axis */
    qint16 value{0}; /**< valor do axis */

    /**
     * @brief Construtor Padrão
     *
     * @param k - Key da tecla
     * @param p - Estado da tecla
     */
    VirtualJoystickAxisEvent(quint8 a,
                             qint16 v){
        axis = a;
        value = v;
    }

    /**
     * @brief Construtor Padrão
     * @details * axis recebe 255
     * * value recebe 0
     *
     */
    VirtualJoystickAxisEvent(){
        // axis = 255
        // value = 0
    }
};

/**
 * @brief Possíveis estados de um VirtualJoystickAxis
 *
 */
enum AxisState : qint8 {

    INCREASE = -1, ///< Crescendo
    STILL = 0, ///< Parado
    DECREASE = 1 ///< Decrescendo

};

/**
 * @brief Struct contendo as informações essenciais de um Axis
 *
 */
struct VirtualJoystickAxis {

    VirtualJoystickAxisEvent event; /**< Evento do axis, contendo o which do axis e seu valor */
    AxisState state{STILL}; /**< Estado do axis */

    /**
     * @brief Construtor padrão
     *
     */
    VirtualJoystickAxis(){
//        event = VirtualJoystickAxisEvent();
//        state = STILL;
    }

    /**
     * @brief Construtor padrão com evento
     * @details state recebe STILL
     * @param e - evento do axis
     */
    VirtualJoystickAxis(VirtualJoystickAxisEvent e){
        event = e;
//        state = STILL;
    }

    ~VirtualJoystickAxis(){
    }
};


const qint16 axisMinimumV { -32767 }; /**< Valor mínimo de um VirtualJoystickAxisEvent */
const qint16 axisMaximumV {  32767 }; /**< Valor máximo de um VirtualJoystickAxisEvent */

#define AXIS_AD 0
#define AXIS_SW 1
#define AXIS_QE 2
#define AXIS_UO 3
#define AXIS_JL 4

/**
 * @brief A classe VirtualJoystick simula um Joystick com as teclas de um teclado
 *
 */
class VirtualJoystick : public QObject
{
    Q_OBJECT

signals:

    ///
    /// \brief Emite evento do tipo VirtualJoystickAxisEvent
    /// \param event - Evento contendo which do axis e seu valor
    ///
    void SIGNAL_axisEvent(const VirtualJoystickAxisEvent& event);

    ///
    /// \brief Emite evento do tipo VirtualJoystickButtonEvent
    /// \param event - Evento contendo key da tecla e seu estado
    ///
    void SIGNAL_buttonEvent(const VirtualJoystickButtonEvent& event);

public:

    /**
     * @brief Construtor padrão da classe VirtualJoystick
     */
    VirtualJoystick (QObject* parent = nullptr);

    /**
     * @brief Destruidor padrão da classe VirtualJoystick
     */
    ~VirtualJoystick();

    void vResetAxis(qint8 id);

    void vResetAxis(const std::initializer_list<qint8>& ids);
private:

    /**
     * @brief Atualiza o valor de um axis em função de seu estado
     */
    void vChangeAxisValue(VirtualJoystickAxis *axis);


    /**
     * @brief Esta função atribui o estado do axis caso necessário
     *
     * @param key - Key da tecla
     * @param pressed - Estado da tecla
     */
    void vReadAxes(const Qt::Key key, const  bool pressed);

    /**
     * @brief Emite um sinal referente ao evento caso a tecla deva ser considerada
     * @details Chama a função vHalt() caso a tecla tenha valor Qt::Key_0
     * @param key - Key da tecla
     * @param pressed - Estado da tecla
     */
    bool bReadButtons(const Qt::Key key, const bool pressed);

    /**
     * @brief Processa o evento recebido
     *
     * @param event - Evento
     * @param pressed - Estado da tecla
     */
    bool bProcessKeyEvent(const QKeyEvent* event, const bool pressed);

    /**
     * @brief Atualiza os valores de todos os axis
     *
     */
    void vUpdateAxis();

    QScopedPointer<QTimer> updateTimer; /**< Timer utilizado para atualizar os valores de todos
 os axis em função de seus estado*/

    QScopedPointer<VirtualJoystickAxis> axisAD; /**< TODO: describe */
    QScopedPointer<VirtualJoystickAxis> axisSW; /**< TODO: describe */
    QScopedPointer<VirtualJoystickAxis> axisQE; /**< TODO: describe */
    QScopedPointer<VirtualJoystickAxis> axisUO; /**< TODO: describe */
    QScopedPointer<VirtualJoystickAxis> axisJL; /**< TODO: describe */

    const QList<Qt::Key> listOfButtons{ /**< Lista de keys da teclas consideradas */
        QList<Qt::Key>{
                    Qt::Key_H,
                    Qt::Key_R,
                    Qt::Key_K,
                    Qt::Key_C
        }
    };

    qint16 axisStep{500}; /**< Passo de variação do valor do axis */


public slots:

    /**
     * @brief Muda o valor da variável axisStep
     *
     * @param newValue - Novo valor atribuído
     */
    void SLOT_setStepValue(const qint16 newValue);

protected:

    /**
     * @brief Trata os eventos de quando as teclas do teclado são pressionadas ou soltas
     * @note Esta função apenas é chamada quando o VirtualJoystick está ativado
     */
    bool eventFilter (QObject* object, QEvent* event);

};

#endif // VIRTUALJOYSTICK_H
