/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#ifndef TESTEGAMEPAD_H
#define TESTEGAMEPAD_H

#include <QDialog>

#include "GamePadJoystick/gamepadjoystick.h"

namespace Ui {
class TesteGamePad;
}

class TesteGamePad : public QDialog
{
    Q_OBJECT

public:
    TesteGamePad(QWidget *parent = nullptr);
    ~TesteGamePad();

    void setGamepadName(const QString name);

public slots:

    void SLOT_getAxisEventGamePad(const GamePadJoystickAxisEvent event);

    void SLOT_getButtonEventGamePad(const GamePadJoystickButtonEvent event);

    void SLOT_getButtonWithValueEventGamePad(const GamePadJoystickButtonWithValueEvent event);

signals:
    void SIGNAL_close();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::TesteGamePad *ui;
};

#endif // TESTEGAMEPAD_H
