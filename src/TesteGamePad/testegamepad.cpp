/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#include "testegamepad.h"
#include "ui_testegamepad.h"

TesteGamePad::TesteGamePad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TesteGamePad)
{
    ui->setupUi(this);
    this->setWindowTitle("Teste gamepad");
}

TesteGamePad::~TesteGamePad()
{
    delete ui;
}

void TesteGamePad::setGamepadName(const QString name)
{
    ui->l_JoystickData->setText(name);
}

void TesteGamePad::SLOT_getAxisEventGamePad(const GamePadJoystickAxisEvent event)
{
    switch(event.axis){
        case QGamepadManager::AxisLeftX:

            ui->pb_Lx->setValue(event.value);
            break;

        case QGamepadManager::AxisLeftY:

            ui->pb_Ly->setValue(-event.value);
            break;

        case QGamepadManager::AxisRightX:

            ui->pb_Rx->setValue(event.value);
            break;

        case QGamepadManager::AxisRightY:

            ui->pb_Ry->setValue(event.value);
            break;

        default:
            break;
    }

}

void TesteGamePad::SLOT_getButtonEventGamePad(const GamePadJoystickButtonEvent event)
{
    switch (event.button) {

        case QGamepadManager::ButtonA:

            ui->cb_A->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonB:

            ui->cb_B->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonX:

            ui->cb_X->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonY:

            ui->cb_Y->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonL1:

            ui->cb_L1->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonR1:

            ui->cb_R1->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonL2:
            break;

        case QGamepadManager::ButtonR2:
            break;

        case QGamepadManager::ButtonSelect:

            ui->cb_Select->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonStart:

            ui->cb_Start->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonL3:

            ui->cb_L3->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonR3:

            ui->cb_R3->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonUp:

            ui->cb_Up->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonDown:

            ui->cb_Down->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonRight:

            ui->cb_Right->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonLeft:

            ui->cb_Left->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonCenter:

            ui->cb_Center->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonGuide:

            ui->cb_Guide->setChecked(event.pressed);
            break;

        case QGamepadManager::ButtonInvalid:
            break;
    }

}

void TesteGamePad::SLOT_getButtonWithValueEventGamePad(const GamePadJoystickButtonWithValueEvent event)
{
    switch (event.button) {

        case QGamepadManager::ButtonL2:

            ui->pb_L2->setValue(event.value);
            break;

        case QGamepadManager::ButtonR2:

            ui->pb_R2->setValue(event.value);
            break;

        default:
            break;
    }
}

void TesteGamePad::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event)

    emit SIGNAL_close();
}
