/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#include "velocityviewerwidget.h"

#include <QtMath>
#include <QPainter>
#include <QDebug>

#define ROBOT_FRONT_ANGLE 45

namespace VelocityViewerCommon
{

void drawArrowEnd(QPainter* painter, const QPointF endPoint,
                  const float length, const float directionAngle,
                  const float angleDegrees = 50)
{
    const float angle0 = qDegreesToRadians(directionAngle + 180.f - angleDegrees/2);
    const QPointF point0 = endPoint + length*QPointF(qCos(angle0), qSin(angle0));

    const float angle1 = qDegreesToRadians(directionAngle + 180.f + angleDegrees/2);
    const QPointF point1 = endPoint + length*QPointF(qCos(angle1), qSin(angle1));

    painter->drawLine(endPoint, point0);
    painter->drawLine(endPoint, point1);
}

}

VelocityViewerWidget::VelocityViewerWidget(QWidget *parent) :
    QWidget(parent),
    m_backgroundColor( Qt::transparent ),
    m_robotBackgroundColor( QColor(30, 30, 30) ),
    m_velocityVectorColor( Qt::green ),
    m_xVelocityVectorColor( Qt::yellow ),
    m_yVelocityVectorColor( Qt::cyan ),
    m_rotationVectorColor( Qt::magenta ),
    m_penThickness( 2 ),
    m_vx( 0 ),
    m_vy( 0 ),
    m_vw( 0 )
{

}

void VelocityViewerWidget::setVw(double newVw)
{
    m_vw = newVw;
}

void VelocityViewerWidget::setVy(double newVy)
{
    m_vy = newVy;
}

void VelocityViewerWidget::setVx(double newVx)
{
    m_vx = newVx;
}

void VelocityViewerWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter(this);

    painter.fillRect(this->rect(), m_backgroundColor);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);

    // Draw the robot base
    const int diameter = qMin(width(), height())/1.25;
    const QRect robotRect = QRect(-diameter/2, -diameter/2, diameter, diameter);

    painter.setPen(Qt::NoPen);
    painter.setBrush(m_robotBackgroundColor);

    painter.drawChord(robotRect, 16*(90 + ROBOT_FRONT_ANGLE), 16*(360 - 2*ROBOT_FRONT_ANGLE));

    // Draw linear velocity vectors
    const QPointF vxEnd = QPointF(diameter*m_vx/sqrt(8), 0);
    const QPointF vyEnd = QPointF(0, -diameter*m_vy/sqrt(8));
    const QPointF vEnd  = vyEnd + vxEnd;

    painter.setPen(QPen(m_xVelocityVectorColor, m_penThickness, Qt::DashLine, Qt::RoundCap));
    painter.setBrush(Qt::NoBrush);

    painter.drawLine(QPointF(0,0), vxEnd);
    VelocityViewerCommon::drawArrowEnd(
                &painter, vxEnd, qAbs(vxEnd.x()/5), qRadiansToDegrees(qAtan2(vxEnd.y(), vxEnd.x())));

    painter.setPen(QPen(m_yVelocityVectorColor, m_penThickness, Qt::DashLine, Qt::RoundCap));
    painter.drawLine(QPointF(0,0), vyEnd);
    VelocityViewerCommon::drawArrowEnd(
                &painter, vyEnd, qAbs(vyEnd.y()/5), qRadiansToDegrees(qAtan2(vyEnd.y(), vyEnd.x())));

    painter.setPen(QPen(m_velocityVectorColor, m_penThickness, Qt::SolidLine, Qt::RoundCap));
    painter.drawLine(QPointF(0,0), vEnd);
    VelocityViewerCommon::drawArrowEnd(
                &painter, vEnd, qSqrt(qPow(vEnd.x(), 2) + qPow(vEnd.y(), 2))/5,
                qRadiansToDegrees(qAtan2(vEnd.y(), vEnd.x())));

    // Draw rotation vector
    painter.setPen(QPen(m_rotationVectorColor, m_penThickness, Qt::SolidLine, Qt::RoundCap));
    painter.drawArc(QRect(-1.125*diameter/2, -1.125*diameter/2, 1.125*diameter, 1.125*diameter), 270*16, 180*16*m_vw);

    if(m_vw > 0)
    {
        const float directionAngle = -180*m_vw;
        const float endPointAngle = qDegreesToRadians(directionAngle + 90);
        const QPointF endPoint = QPointF(qCos(endPointAngle), qSin(endPointAngle))*1.125*diameter/2;

        VelocityViewerCommon::drawArrowEnd(
                    &painter, endPoint, qAbs(diameter*m_vw/10), directionAngle);
    }
    else
    {
        const float directionAngle = 180*(1 - m_vw);
        const float endPointAngle = qDegreesToRadians(directionAngle - 90);
        const QPointF endPoint = QPointF(qCos(endPointAngle), qSin(endPointAngle))*1.125*diameter/2;

        VelocityViewerCommon::drawArrowEnd(
                    &painter, endPoint, qAbs(diameter*m_vw/10), directionAngle);
    }

}
