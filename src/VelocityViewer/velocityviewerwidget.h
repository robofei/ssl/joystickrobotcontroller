/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#ifndef VELOCITYVIEWERWIDGET_H
#define VELOCITYVIEWERWIDGET_H

#include <QWidget>

class VelocityViewerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VelocityViewerWidget(QWidget *parent = nullptr);

    void setVx(double newVx);

    void setVy(double newVy);

    void setVw(double newVw);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    QColor m_backgroundColor;
    QColor m_robotBackgroundColor;
    QColor m_velocityVectorColor;
    QColor m_xVelocityVectorColor;
    QColor m_yVelocityVectorColor;
    QColor m_rotationVectorColor;
    int m_penThickness;

    double m_vx;
    double m_vy;
    double m_vw;

};

#endif // VELOCITYVIEWERWIDGET_H
