﻿/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#ifndef INTERFACEJOYSTICK_H
#define INTERFACEJOYSTICK_H

#include <QMainWindow>

#include "GamePadJoystick/gamepadjoystick.h"
#include "VirtualJoystick/virtualjoystick.h"
#include "Radio/radiobase.h"
#include "Robot/robot.h"
#include "TesteGamePad/testegamepad.h"
#include <Eigen/Core>


enum JoystickAtual : quint8{
    NONE,
    VIRTUAL_JOYSTICK,
    GAMEPAD_JOYSTICK
};

namespace Ui {
class InterfaceJoystick;
}




/**
 * @brief
 *
 */
class InterfaceJoystick : public QMainWindow
{
    Q_OBJECT

public:

    /**
     * @brief
     *
     * @param parent
     */
    explicit InterfaceJoystick(QWidget *parent = nullptr);
    /**
     * @brief
     *
     */
    ~InterfaceJoystick();

    void vSaidaDaMensagem(const QtMsgType type,
                          const QMessageLogContext &context,
                          const QString &msg);


private:

    JoystickAtual joystickAtual {JoystickAtual::NONE}; /**< TODO: describe */

    /**
     * @brief
     *
     */
    void fillPortsParameters();


    /**
     * @brief
     *
     * @return int
     */

    /**
     * @brief
     *
     * @param baterryLevel
     */
    void vSetBaterryLevelInterface(float baterryLevel);
    /**
     * @brief
     *
     * @param kickSensor
     */
    void vSetKickSensorInterface(bool kickSensor);

    /**
     * @brief
     *
     * @param b
     */
    void vSetRollerOnOffInterface (bool b);
    /**
     * @brief
     *
     * @param k
     */
    void vSetKICK_TYPE_Interface (KICKTYPE type);

    /**
     * @brief
     *
     * @param dRoda_0_r
     * @param dRoda_1_r
     * @param dRoda_2_r
     * @param dRoda_3_r
     * @param dRoda_0_t
     * @param dRoda_1_t
     * @param dRoda_2_t
     * @param dRoda_3_t
     */
    void vUpdateGraficoInterface(int dRoda_0_r,
                                int dRoda_1_r,
                                int dRoda_2_r,
                                int dRoda_3_r,
                                int dRoda_0_t,
                                int dRoda_1_t,
                                int dRoda_2_t,
                                int dRoda_3_t);


    bool bConectouRadio();

    void vDesconectarRadio();


private slots:

    void SLOT_enableDisableRobotCommands_Virtual();

    void SLOT_enableDisableRobotCommands_Gamepad();


    void SLOT_updatePorts();

    void SLOT_getAxisEventVirtual(const VirtualJoystickAxisEvent& event);

    void SLOT_getButtonEventVirtual(const VirtualJoystickButtonEvent& event);

    void SLOT_conectarGamePad();

    void SLOT_getAxisEventGamePad(const GamePadJoystickAxisEvent event);

    void SLOT_getButtonEventGamePad(const GamePadJoystickButtonEvent event);

    void SLOT_getButtonWithValueEventGamePad(const GamePadJoystickButtonWithValueEvent event);

    void SLOT_enviaComandosParaORadio();

    void SLOT_conectarDesconectarRadio();

    void SLOT_recebeComandosDoRadio();

    void SLOT_iniciaTesteComOGamepad();

    void SLOT_finalizaTesteComOGamepad();

    void on_actionAbout_triggered();

    void on_actionGitLab_Main_Page_triggered();

    void on_cb_velocityViewer_stateChanged(int arg1);

    void on_cb_odometryLegend_stateChanged(int arg1);

    void on_cb_odometry_stateChanged(int arg1);

    void on_cb_odometryWheel0_stateChanged(int arg1);
    void on_cb_odometryWheel1_stateChanged(int arg1);
    void on_cb_odometryWheel2_stateChanged(int arg1);
    void on_cb_odometryWheel3_stateChanged(int arg1);

    void on_sb_Vx_valueChanged(int arg1);

    void on_sb_Vy_valueChanged(int arg1);

    void on_spinBox_3_valueChanged(int arg1);

signals:

    /**
     * @brief
     *
     */
    void SIGNAL_enableVirtualJoystick();
    /**
     * @brief
     *
     */
    void SIGNAL_enableGamePadJoystick();
    /**
     * @brief
     *
     */
    void SIGNAL_disableVirtualJoystick();
    /**
     * @brief
     *
     */
    void SIGNAL_disableGamePadJoystick();

    /**
     * @brief
     *
     * @param double
     */
    void SIGNAL_passoAxisMudou(double);


private:
    Ui::InterfaceJoystick *ui; /**< TODO: describe */


    const int iControle = 100; /**< TODO: describe */

    QScopedPointer<Robot> robot;
    QScopedPointer<VirtualJoystick> virtualJoystick;
    QScopedPointer<GamePadJoystick> gamepad;
    QScopedPointer<RadioBase> radio;

    QVector<double> vel_roda_0_r; /**< TODO: describe */
    QVector<double> vel_roda_1_r; /**< TODO: describe */
    QVector<double> vel_roda_2_r; /**< TODO: describe */
    QVector<double> vel_roda_3_r; /**< TODO: describe */

    QVector<double> vel_roda_0_t; /**< TODO: describe */
    QVector<double> vel_roda_1_t; /**< TODO: describe */
    QVector<double> vel_roda_2_t; /**< TODO: describe */
    QVector<double> vel_roda_3_t; /**< TODO: describe */


    QVector<double> vt_time_graph; /**< TODO: describe */
    int i_time_graph; /**< TODO: describe */

    bool bRadioConectado = false; /**< TODO: describe */
    QScopedPointer<QTimer> timerUpdateRadio;
    QScopedPointer<TesteGamePad> testeGamePad;

    bool bFechar { false };
    bool bHalt { false };
    qint8 iVelocidadeMaximaGamepad{100};

    int iFatorVelocidadeGrafico; /**< TODO: describe */
    int iContadorVelocidadeGrafico = 0; /**< TODO: describe */


    qint8 iConverteEscalaAxis_100_100(qint16 iAxis);
    qint8 iConverteEscalaAxis_0_100(qint16 iAxis);
    qint8 iConverteEscalaButtonWithValue_0_100(qint16 iAxis);
    QVector4D vt4dConverteLinearRotacao(QVector2D vt2dVel, float fVw);
    void vReiniciaGrafico();
    void vIniciaGrafico();

    void configureDarkStyle();

};

#endif // INTERFACEJOYSTICK_H
