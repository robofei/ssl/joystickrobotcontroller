/*****************************************************************************
 * Joystick Robot Controller                                                 *
 * Copyright (C) 2021-2022  Bruno Bollos Correa                              *
 * Copyright (C) 2021-2022  RoboFEI / Small Size League                      *
 *                                                                           *
 * This program is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************
 */


#include "interfacejoystick.h"
#include "ui_interfacejoystick.h"

#include "Version/version.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QShortcut>
#include <QDesktopServices>

#define Y_RANGE 1000

InterfaceJoystick::InterfaceJoystick(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::InterfaceJoystick)
{
    ui->setupUi(this);
    ui->cp_Odometria->setInteraction(QCP::Interaction::iRangeZoom, true);
    ui->cp_Odometria->setInteraction(QCP::Interaction::iRangeDrag, true);

    QLoggingCategory::setFilterRules(QStringLiteral("qt.gamepad.debug=true"));

    robot.reset( new Robot() );

    robot->hw_version = v2013;
    radio.reset( new RadioBase() );
    radio->portConnections();

    timerUpdateRadio.reset( new QTimer(this) );


    connect(ui->robotCommand_XBox, &QCheckBox::clicked,
            this, &InterfaceJoystick::SLOT_enableDisableRobotCommands_Gamepad);

    connect(ui->robotCommand_Virtual, &QCheckBox::clicked,
            this, &InterfaceJoystick::SLOT_enableDisableRobotCommands_Virtual);


    connect(ui->connectDisconnectButton, &QPushButton::clicked,
            this, &InterfaceJoystick::SLOT_conectarDesconectarRadio);


    connect(ui->refreshButton, &QPushButton::clicked,
            this, &InterfaceJoystick::SLOT_updatePorts);

    connect(ui->pb_ConectarGamePad, &QPushButton::clicked,
            this, &InterfaceJoystick::SLOT_conectarGamePad);


    connect(timerUpdateRadio.get(), &QTimer::timeout,
            this, &InterfaceJoystick::SLOT_enviaComandosParaORadio);


    connect(radio->serial, &QSerialPort::readyRead,
            this, &InterfaceJoystick::SLOT_recebeComandosDoRadio);


    // Switch tabs shortcut
    connect(new QShortcut(QKeySequence(Qt::ALT | Qt::Key_1), this), &QShortcut::activated,
                          this, [this](){ ui->tabWidget->setCurrentIndex(0); });
    connect(new QShortcut(QKeySequence(Qt::ALT | Qt::Key_2), this), &QShortcut::activated,
                          this, [this](){ ui->tabWidget->setCurrentIndex(1); });

    // Close app with CTRL + Q
    connect(new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_Q), this), &QShortcut::activated,
                          this, &InterfaceJoystick::close);

    ui->gb_RobotStatus->hide();
    ui->gb_options->hide();
    ui->wg_progressos->hide();
    ui->w_encoders->hide();

    ui->velocityViewer->hide();
    ui->cp_Odometria->hide();

    ui->w_KICK_Roller->hide();
    fillPortsParameters();

    ui->pB_Vx->setRange(-100,100);
    ui->pB_Vy->setRange(-100,100);
    ui->pB_W->setRange(-100,100);

    ui->pB_velMaxima->setRange(0, 100);
    ui->pB_ForcaChute->setRange(0, 100);
    ui->pB_Velocidade_Roller->setRange(0, 100);

    ui->cb_HALT->setEnabled(false);
    ui->cb_KICK_CUSTOM->setEnabled(false);
    ui->cb_Ativa_Roller->setEnabled(false);
    ui->cb_CHIP_KICK_STRONG->setEnabled(false);

    ui->cb_HALT->setChecked(false);
    ui->cb_KICK_CUSTOM->setChecked(false);
    ui->cb_Ativa_Roller->setChecked(false);
    ui->cb_CHIP_KICK_STRONG->setChecked(false);

    ui->robotCommand_XBox->setEnabled(false);

    ui->connectDisconnectButton->setEnabled(true);


    vIniciaGrafico();

    {
        ui->hs_VelocidadeGrafico->setMinimum(1);
        ui->hs_VelocidadeGrafico->setMaximum(20);

        ui->hs_Sensibilidade->setMinimum(50);
        ui->hs_Sensibilidade->setMaximum(1000);
        ui->hs_Sensibilidade->setSingleStep(50);

        connect(ui->hs_VelocidadeGrafico, &QSlider::valueChanged,
                this, [this](qint16 value){  iFatorVelocidadeGrafico = ui->hs_VelocidadeGrafico->maximum() - value;  });

        ui->hs_VelocidadeGrafico->setValue(20);

        ui->hs_Sensibilidade->setValue(500);
    }

    ui->baudRateBox->setCurrentText(QStringLiteral("230400"));

    this->setWindowTitle("Joystick Robot Controller RoboFEI-SSL");
    this->setWindowIcon(QIcon("://icon/joystickrobotcontroller_icon.svg"));
    this->setAttribute(Qt::WA_DeleteOnClose);

    configureDarkStyle();

    connect(ui->pb_testeGamepad, &QPushButton::pressed,
            this, &InterfaceJoystick::SLOT_iniciaTesteComOGamepad);

    vSetBaterryLevelInterface(0);
    vSetKickSensorInterface(false);

    // Tenta conectar o gamepad ao iniciar o programa
    SLOT_conectarGamePad();
}

InterfaceJoystick::~InterfaceJoystick()
{
    bFechar = true;

    qApp->quit();
}

void InterfaceJoystick::vSaidaDaMensagem(const QtMsgType type,
                                         const QMessageLogContext &context,
                                         const QString &msg)
{
    Q_UNUSED(context)

    if(bFechar)
        return;

    QListWidgetItem *item = new QListWidgetItem();
    switch (type) {

        case QtDebugMsg:

            item->setForeground(Qt::cyan);
            qDebug()<< "Debug: " << qPrintable(msg);

            break;
        case QtInfoMsg:

            qDebug()<< "Info: " << qPrintable(msg);
            item->setForeground(Qt::green);

            break;
        case QtWarningMsg:

            qDebug()<< "Warning: " << qPrintable(msg);
            item->setForeground(Qt::yellow);

            break;
        case QtCriticalMsg:

            qDebug()<< "Critical: " << qPrintable(msg);
            item->setForeground(Qt::red);

            break;
        case QtFatalMsg:
            return;
    }


    item->setText(msg);


    if(ui->list_w_mensagens->count() > 50)
        delete ui->list_w_mensagens->takeItem(ui->list_w_mensagens->count() - 1);

    ui->list_w_mensagens->insertItem(0, item);
}

void InterfaceJoystick::SLOT_enableDisableRobotCommands_Virtual(){

    if(ui->robotCommand_Virtual->isChecked())
    {
        bHalt = false;
        vSetKICK_TYPE_Interface(KICK_NONE);
        vSetRollerOnOffInterface(false);

        ui->robotCommand_XBox->hide();
        ui->pb_ConectarGamePad->hide();

        if(ui->cb_odometry->isChecked())
            ui->cp_Odometria->show();
        if(ui->cb_velocityViewer->isChecked())
            ui->velocityViewer->show();

        ui->gb_RobotStatus->show();
        ui->gb_options->show();
        ui->wg_progressos->show();
        ui->w_KICK_Roller->show();

        ui->l_ForcaChute_e->show();
        ui->l_ForcaChute->show();
        ui->pB_ForcaChute->show();
        ui->l_ForcaChute_d->show();

        ui->pB_Vx->setValue(0);
        ui->pB_Vy->setValue(0);
        ui->pB_W->setValue(0);
        ui->pB_ForcaChute->setValue(0);
        ui->pB_Velocidade_Roller->setValue(0);

        ui->l_Vx_e->setPixmap(QIcon(":/Keyboard/a.svg").pixmap(ui->l_Vx_e->size()));
        ui->l_Vx_d->setPixmap(QIcon(":/Keyboard/d.svg").pixmap(ui->l_Vx_d->size()));
        ui->l_Vy_e->setPixmap(QIcon(":/Keyboard/s.svg").pixmap(ui->l_Vy_e->size()));
        ui->l_Vy_d->setPixmap(QIcon(":/Keyboard/w.svg").pixmap(ui->l_Vy_d->size()));
        ui->l_W_e->setPixmap(QIcon(":/Keyboard/q.svg").pixmap(ui->l_W_e->size()));
        ui->l_W_d->setPixmap(QIcon(":/Keyboard/e.svg").pixmap(ui->l_W_d->size()));

        ui->l_ForcaChute_e->setPixmap(QIcon(":/Keyboard/u.svg").pixmap(ui->l_ForcaChute_e->size()));
        ui->l_ForcaChute_d->setPixmap(QIcon(":/Keyboard/o.svg").pixmap(ui->l_ForcaChute_d->size()));

        ui->l_Vel_Roller_e->setPixmap(QIcon(":/Keyboard/j.svg").pixmap(ui->l_Vel_Roller_e->size()));
        ui->l_Vel_Roller_d->setPixmap(QIcon(":/Keyboard/l.svg").pixmap(ui->l_Vel_Roller_d->size()));

        ui->l_HALT->setPixmap(QIcon(":/Keyboard/0.svg").pixmap(ui->l_HALT->size()));
        ui->l_Ativa_Roller->setPixmap(QIcon(":/Keyboard/1.svg").pixmap(ui->l_Ativa_Roller->size()));
        ui->l_KICK_CUSTOM->setPixmap(QIcon(":/Keyboard/4.svg").pixmap(ui->l_KICK_CUSTOM->size()));
        ui->l_CHIP_KICK_STRONG->setPixmap(QIcon(":/Keyboard/5.svg").pixmap(ui->l_CHIP_KICK_STRONG->size()));

        ui->w_halt->show();
        ui->w_chip_strong->show();
        ui->w_kick_custom->show();
        ui->w_encoders->show();
        ui->w_ativar_roller->show();

        ui->l_velMaxima->hide();
        ui->l_velMaxima_e->hide();
        ui->l_velMaxima_d->hide();
        ui->pB_velMaxima->hide();

        joystickAtual = JoystickAtual::VIRTUAL_JOYSTICK;

        virtualJoystick.reset(new VirtualJoystick(this));

        connect(virtualJoystick.get(), &VirtualJoystick::SIGNAL_axisEvent,
                this, &InterfaceJoystick::SLOT_getAxisEventVirtual);

        connect(virtualJoystick.get(), &VirtualJoystick::SIGNAL_buttonEvent,
                this, &InterfaceJoystick::SLOT_getButtonEventVirtual);

        connect(ui->hs_Sensibilidade, &QSlider::valueChanged,
                virtualJoystick.get(), &VirtualJoystick::SLOT_setStepValue);
    }
    else
    {
        ui->robotCommand_XBox->show();
        ui->pb_ConectarGamePad->show();

        ui->cp_Odometria->hide();
        ui->velocityViewer->hide();

        ui->gb_RobotStatus->hide();
        ui->gb_options->hide();
        ui->wg_progressos->hide();
        ui->w_kick_custom->hide();
        ui->w_chip_strong->hide();
        ui->w_KICK_Roller->hide();
        ui->w_encoders->hide();
        joystickAtual = JoystickAtual::NONE;

        qApp->removeEventFilter(virtualJoystick.get());

        virtualJoystick.reset();

    }
}
void InterfaceJoystick::SLOT_enableDisableRobotCommands_Gamepad(){

    if(ui->robotCommand_XBox->isChecked())
    {
        bHalt = false;
        iVelocidadeMaximaGamepad = 100;
        vSetKICK_TYPE_Interface(KICK_NONE);
        vSetRollerOnOffInterface(false);

        ui->robotCommand_Virtual->hide();
        ui->pb_ConectarGamePad->hide();

        if(ui->cb_odometry->isChecked())
            ui->cp_Odometria->show();
        if(ui->cb_velocityViewer->isChecked())
            ui->velocityViewer->show();

        ui->gb_RobotStatus->show();
        ui->gb_options->show();

        ui->w_KICK_Roller->show();
        ui->wg_progressos->show();

        ui->l_velMaxima->show();
        ui->l_velMaxima_e->show();
        ui->l_velMaxima_d->show();
        ui->pB_velMaxima->show();

        ui->pB_Vx->setValue(0);
        ui->pB_Vy->setValue(0);
        ui->pB_W->setValue(0);
        ui->pB_velMaxima->setValue(iVelocidadeMaximaGamepad);         /// velocidade máxima
        ui->pB_Velocidade_Roller->setValue(0);

        ui->l_Vx_e->setPixmap(QIcon(":/XBox/Xbox_Left_stick_button_c.svg").
                              pixmap(ui->l_Vx_e->size()));
        ui->l_Vx_d->setPixmap(QIcon(":/XBox/Xbox_Left_stick_button_a.svg").
                              pixmap(ui->l_Vx_d->size()));
        ui->l_Vy_e->setPixmap(QIcon(":/XBox/Xbox_Left_stick_button_b.svg").
                              pixmap(ui->l_Vy_e->size()));
        ui->l_Vy_d->setPixmap(QIcon(":/XBox/Xbox_Left_stick_button_d.svg").
                              pixmap(ui->l_Vy_d->size()));
        ui->l_W_e->setPixmap(QIcon(":/XBox/Xbox_Right_stick_button_c.svg").
                              pixmap(ui->l_W_e->size()));
        ui->l_W_d->setPixmap(QIcon(":/XBox/Xbox_Right_stick_button_a.svg").
                              pixmap(ui->l_W_d->size()));

        ui->l_Vel_Roller_e->setText("");
        ui->l_Vel_Roller_d->setPixmap(QIcon(":/XBox/Xbox_Right_Trigger.svg").
                              pixmap(ui->l_Vel_Roller_d->size()));

        ui->l_velMaxima_e->setPixmap(QIcon(":/XBox/Xbox_Back_button.svg").
                                      pixmap(ui->l_ForcaChute_e->size()));
        ui->l_velMaxima_d->setPixmap(QIcon(":/XBox/Xbox_Start_button.svg").
                                      pixmap(ui->l_ForcaChute_d->size()));


        ui->l_KICK_CUSTOM->setPixmap(QIcon(":/XBox/Xbox_button_A.svg").
                              pixmap(ui->l_KICK_CUSTOM->size()));
        ui->l_CHIP_KICK_STRONG->setPixmap(QIcon(":/XBox/Xbox_button_B.svg").
                              pixmap(ui->l_CHIP_KICK_STRONG->size()));

        ui->w_halt->hide();
        ui->w_chip_strong->show();
        ui->w_kick_custom->show();
        ui->w_encoders->show();
        ui->w_ativar_roller->hide();

        ui->l_ForcaChute->hide();
        ui->l_ForcaChute_e->hide();
        ui->l_ForcaChute_d->hide();
        ui->pB_ForcaChute->hide();

        joystickAtual = JoystickAtual::GAMEPAD_JOYSTICK;


        connect(this, &InterfaceJoystick::SIGNAL_disableGamePadJoystick,
                gamepad.get(), &GamePadJoystick::SLOT_disableGamePadJoystick);

        connect(this, &InterfaceJoystick::SIGNAL_enableGamePadJoystick,
                gamepad.get(), &GamePadJoystick::SLOT_enableGamePadJoystick);

        connect(gamepad.get(), &GamePadJoystick::SIGNAL_axisEvent,
                this, &InterfaceJoystick::SLOT_getAxisEventGamePad);

        connect(gamepad.get(), &GamePadJoystick::SIGNAL_buttonEvent,
                this, &InterfaceJoystick::SLOT_getButtonEventGamePad);

        connect(gamepad.get(), &GamePadJoystick::SIGNAL_buttonWithValueEvent,
                this, &InterfaceJoystick::SLOT_getButtonWithValueEventGamePad);

        emit SIGNAL_enableGamePadJoystick();
    }
    else if(!ui->robotCommand_XBox->isChecked()){

        ui->robotCommand_Virtual->show();
        ui->pb_ConectarGamePad->show();

        ui->cp_Odometria->hide();
        ui->velocityViewer->hide();

        ui->gb_RobotStatus->hide();
        ui->gb_options->hide();
        ui->wg_progressos->hide();
        ui->w_encoders->hide();
        ui->w_KICK_Roller->hide();

        joystickAtual = JoystickAtual::NONE;

        emit SIGNAL_disableGamePadJoystick();
    }

}

void InterfaceJoystick::fillPortsParameters(){
    ui->portNameBox->clear();
    ui->baudRateBox->clear();

    QList<QSerialPortInfo> avPorts=QSerialPortInfo::availablePorts();

    ui->baudRateBox->setInsertPolicy(QComboBox::NoInsert);
    ui->baudRateBox->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    ui->baudRateBox->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->baudRateBox->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->baudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->baudRateBox->addItem(QStringLiteral("230400"), 230400);


    for (const QSerialPortInfo &info : avPorts)
    {
        QStringList list;
        list << info.portName();
        ui->portNameBox->addItem(list.first(), list);
    }
}



void InterfaceJoystick::SLOT_updatePorts(){
    ui->portNameBox->clear();

    QList<QSerialPortInfo> portasDisponiveis = QSerialPortInfo::availablePorts();

    for(const QSerialPortInfo &info : portasDisponiveis)
    {
        QStringList list;
        list << info.portName();
        ui->portNameBox->addItem(list.first(), list);
    }

}

void InterfaceJoystick::SLOT_getAxisEventVirtual(const VirtualJoystickAxisEvent &event)
{
    if(bHalt)
        return;

    switch (event.axis)
    {
        case AXIS_AD:
            ui->pB_Vx->setValue(iConverteEscalaAxis_100_100( event.value));
            break;

        case AXIS_SW:
            ui->pB_Vy->setValue(iConverteEscalaAxis_100_100(event.value));
            break;

        case AXIS_QE:
            ui->pB_W->setValue(iConverteEscalaAxis_100_100(event.value));
            break;

        case AXIS_UO:
            ui->pB_ForcaChute->setValue(iConverteEscalaAxis_0_100(event.value));
            break;

        case AXIS_JL:
            ui->pB_Velocidade_Roller->setValue(iConverteEscalaAxis_0_100(event.value));
            break;

        default:
            break;
    }

    if(ui->velocityViewer->isVisible())
    {
        ui->velocityViewer->setVx(ui->pB_Vx->value()/100.);
        ui->velocityViewer->setVy(ui->pB_Vy->value()/100.);
        ui->velocityViewer->setVw(ui->pB_W->value()/100.);
        ui->velocityViewer->update();
    }
}

void InterfaceJoystick::SLOT_getButtonEventVirtual(const VirtualJoystickButtonEvent &event)
{
    if(bHalt && event.key != Qt::Key_H)
        return;

    switch (event.key) {

        case Qt::Key_H:
            ui->cb_HALT->setChecked(event.pressed);
            bHalt = event.pressed;

            if(event.pressed){
                ui->cb_Ativa_Roller->setChecked(false);
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                ui->cb_KICK_CUSTOM->setChecked(false);
                vSetRollerOnOffInterface(false);
                vSetKICK_TYPE_Interface(KICK_NONE);
                virtualJoystick->vResetAxis({AXIS_AD, AXIS_SW, AXIS_QE});
                ui->pB_Vx->setValue(0);
                ui->pB_Vy->setValue(0);
                ui->pB_W->setValue(0);


                if(ui->velocityViewer->isVisible())
                {
                    ui->velocityViewer->setVx(ui->pB_Vx->value()/100.);
                    ui->velocityViewer->setVy(ui->pB_Vy->value()/100.);
                    ui->velocityViewer->setVw(ui->pB_W->value()/100.);
                    ui->velocityViewer->update();
                }
            }
            break;

        case Qt::Key_R:
            if(!event.pressed)
            {
                ui->cb_Ativa_Roller->setChecked(!ui->cb_Ativa_Roller->isChecked());
                vSetRollerOnOffInterface(ui->cb_Ativa_Roller->isChecked());
            }
            break;

        case Qt::Key_K:
            ui->cb_KICK_CUSTOM->setChecked(event.pressed);

            if(event.pressed){
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                vSetKICK_TYPE_Interface(KICK_CUSTOM);
            }
            else {
                vSetKICK_TYPE_Interface(KICK_NONE);
            }

            break;

        case Qt::Key_C:
            ui->cb_CHIP_KICK_STRONG->setChecked(event.pressed);

            if(event.pressed){
                ui->cb_KICK_CUSTOM->setChecked(false);
                vSetKICK_TYPE_Interface(CHIP_KICK_CUSTOM);
            }
            else {
                vSetKICK_TYPE_Interface(KICK_NONE);
            }

            break;

        default:
            break;
    }

}

void InterfaceJoystick::SLOT_conectarGamePad()
{
    gamepad.reset( new GamePadJoystick() );
    ui->robotCommand_XBox->setEnabled(gamepad->bGamePadConnected());

}

void InterfaceJoystick::SLOT_getAxisEventGamePad(const GamePadJoystickAxisEvent event)
{
    if(bHalt)
        return;

    const qint8 ZONA_MORTA = 20;

    const qint8 evValue = qAbs(event.value) < ZONA_MORTA ?
                              0 :
                              100./(100-ZONA_MORTA) *
                              (event.value - ZONA_MORTA*event.value/qAbs(event.value));

    const float fatorVelMaxima = ui->pB_velMaxima->value()/100.;

    switch (event.axis)
    {
        case QGamepadManager::AxisLeftX:
            ui->pB_Vx->setValue(fatorVelMaxima*evValue);
            break;

        case QGamepadManager::AxisLeftY:
            ui->pB_Vy->setValue(-fatorVelMaxima*evValue);
            break;

        case QGamepadManager::AxisRightX:
            ui->pB_W->setValue(fatorVelMaxima*evValue);
            break;

        case QGamepadManager::AxisRightY:
            break;

        default:
            break;
    }

    if(ui->velocityViewer->isVisible())
    {
        ui->velocityViewer->setVx(ui->pB_Vx->value()/100.);
        ui->velocityViewer->setVy(ui->pB_Vy->value()/100.);
        ui->velocityViewer->setVw(ui->pB_W->value()/100.);
        ui->velocityViewer->update();
    }
}

void InterfaceJoystick::SLOT_getButtonEventGamePad(const GamePadJoystickButtonEvent event)
{
    if(bHalt && event.button != QGamepadManager::ButtonL1)
        return;

    switch (event.button) {

        case QGamepadManager::ButtonA:
            ui->cb_KICK_CUSTOM->setChecked(event.pressed);

            if(event.pressed){
                ui->cb_CHIP_KICK_STRONG->setChecked(false);
                vSetKICK_TYPE_Interface(KICK_CUSTOM);
            }
            else {
                vSetKICK_TYPE_Interface(KICK_NONE);
            }

            break;

        case QGamepadManager::ButtonB:
            ui->cb_CHIP_KICK_STRONG->setChecked(event.pressed);

            if(event.pressed){
                ui->cb_KICK_CUSTOM->setChecked(false);
                vSetKICK_TYPE_Interface(CHIP_KICK_CUSTOM);
            }
            else {
                vSetKICK_TYPE_Interface(KICK_NONE);
            }

            break;

        case QGamepadManager::ButtonY:
            break;

        case QGamepadManager::ButtonL1:
            break;

        case QGamepadManager::ButtonR1:
            break;

        case QGamepadManager::ButtonL2:
            break;

        case QGamepadManager::ButtonR2:
            break;

        case QGamepadManager::ButtonSelect:

            if(!event.pressed)
                return;

            iVelocidadeMaximaGamepad -= 5;

            if(iVelocidadeMaximaGamepad < 0)
                iVelocidadeMaximaGamepad = 0;

            ui->pB_velMaxima->setValue(iVelocidadeMaximaGamepad);

            break;

        case QGamepadManager::ButtonStart:

            if(!event.pressed)
                return;

            iVelocidadeMaximaGamepad += 5;

            if(iVelocidadeMaximaGamepad > 100)
                iVelocidadeMaximaGamepad = 100;

            ui->pB_velMaxima->setValue(iVelocidadeMaximaGamepad);
            break;

        case QGamepadManager::ButtonL3:
            break;

        case QGamepadManager::ButtonR3:
            break;

        case QGamepadManager::ButtonUp:
            break;

        case QGamepadManager::ButtonDown:
            break;

        case QGamepadManager::ButtonRight:
            break;

        case QGamepadManager::ButtonLeft:
            break;

        case QGamepadManager::ButtonCenter:
        case QGamepadManager::ButtonGuide:
            break;

        default:
            break;


    }
}

void InterfaceJoystick::SLOT_getButtonWithValueEventGamePad(const GamePadJoystickButtonWithValueEvent event)
{
    if(bHalt)
        return;

    switch (event.button) {

        case QGamepadManager::ButtonL2:

            break;

        case QGamepadManager::ButtonR2:
        {
            const int ZONA_MORTA = 10;

            const int rollerVal =
                    event.value < ZONA_MORTA ?
                        0 :
                        100*(event.value - ZONA_MORTA)/(100-ZONA_MORTA);

            ui->pB_Velocidade_Roller->setValue(rollerVal);
            vSetRollerOnOffInterface(rollerVal);
            break;
        }
        default:
            break;
    }
}

void InterfaceJoystick::SLOT_enviaComandosParaORadio()
{

    robot->id = ui->sb_robotID->value();

    switch (joystickAtual) {

        case NONE:

            robot->cmd_vx = 0;
            robot->cmd_vy = 0;
            robot->cmd_w = 0;

            robot->cmd_rollerspd = 0;
            robot->kickStrength = 0;

            robot->kick = KICK_NONE;

            break;

        case VIRTUAL_JOYSTICK:

            robot->cmd_vx = ui->pB_Vx->value();
            robot->cmd_vy = ui->pB_Vy->value();
            robot->cmd_w = ui->pB_W->value();

            robot->cmd_rollerspd = ui->pB_Velocidade_Roller->value();

            if (ui->cb_KICK_CUSTOM->isChecked())
                robot->kick = KICK_CUSTOM;

            else if(ui->cb_CHIP_KICK_STRONG->isChecked())
                robot->kick = CHIP_KICK_CUSTOM;

            else
                robot->kick = KICK_NONE;

            robot->kickStrength = ui->pB_ForcaChute->value();

            break;

        case GAMEPAD_JOYSTICK:

            robot->cmd_vx = ui->pB_Vx->value();
            robot->cmd_vy = ui->pB_Vy->value();
            robot->cmd_w = ui->pB_W->value();

            robot->cmd_rollerspd = ui->pB_Velocidade_Roller->value();

            if(ui->cb_KICK_CUSTOM->isChecked())
                robot->kick = KICK_CUSTOM;

            else if(ui->cb_CHIP_KICK_STRONG->isChecked())
                robot->kick = CHIP_KICK_CUSTOM;

            else
                robot->kick = KICK_NONE;
            robot->kickStrength = 100;

            break;
    }

    if (ui->cb_hasVision->isChecked())
    {
        robot->x = ui->sb_X->value();
        robot->y = ui->sb_Y->value();
        robot->theta = ui->sb_Theta->value();
    }
    else
    {
        robot->x = 0;
        robot->y = 0;
        robot->theta = 0;
    }

    QByteArray data = radio->RadioPacketize(robot.get());//cria o pacote de dados referente ao robo

    data = radio->encodePacket(robot->id,data);//cria o array com o protocolo a ser enviado

    if(!radio->sendData(data)){
        qWarning()<<"Error while trying to send the packet: " + radio->serial->errorString();
    }


}

void InterfaceJoystick::SLOT_conectarDesconectarRadio()
{
    if(!bRadioConectado){

        if(bConectouRadio()){

            bRadioConectado = true;
            ui->connectDisconnectButton->setText("Disconnect Radio");
        }
    }
    else {
        vDesconectarRadio();
        ui->connectDisconnectButton->setText("Connect Radio");
    }
}

void InterfaceJoystick::SLOT_recebeComandosDoRadio()
{
    radio->monitored_robotid = robot->id;

    QByteArray pacoteRecebido;
    radio->readData(pacoteRecebido);

    float baterrylevel = radio->battery_level[robot->id];
    robot->battery = baterrylevel;

    bool kicksensor = radio->kick_sensor[robot->id];
    robot->kickSensor = kicksensor;

    vSetBaterryLevelInterface(baterrylevel);
    vSetKickSensorInterface(kicksensor);

    QVector2D vt2dVel = QVector2D(robot->cmd_vx, robot->cmd_vy);
    QVector4D velocidade_rodas_T = vt4dConverteLinearRotacao(vt2dVel, robot->cmd_w);

    int velocidade_0_R = 0,
            velocidade_1_R = 0,
            velocidade_2_R = 0,
            velocidade_3_R = 0;

    if(radio->measured_1.size() > 0)
        velocidade_0_R = radio->measured_1.last();

    if(radio->measured_2.size() > 0)
        velocidade_1_R = radio->measured_2.last();

    if(radio->measured_3.size() > 0)
        velocidade_2_R = radio->measured_3.last();

    if(radio->measured_4.size() > 0)
        velocidade_3_R = radio->measured_4.last();

    vUpdateGraficoInterface(velocidade_0_R,
                            velocidade_1_R,
                            velocidade_2_R,
                            velocidade_3_R,
                            static_cast<double>(velocidade_rodas_T.x()),
                            static_cast<double>(velocidade_rodas_T.y()),
                            static_cast<double>(velocidade_rodas_T.z()),
                            static_cast<double>(velocidade_rodas_T.w()));

    ui->lb_encoder_1->setText(QString::number(radio->encoderCount[0]));
    ui->lb_encoder_2->setText(QString::number(radio->encoderCount[1]));
    ui->lb_encoder_3->setText(QString::number(radio->encoderCount[2]));
    ui->lb_encoder_4->setText(QString::number(radio->encoderCount[3]));
}

void InterfaceJoystick::SLOT_iniciaTesteComOGamepad()
{

    gamepad.reset(new GamePadJoystick(this));

    if(gamepad->bGamePadConnected()){

        testeGamePad.reset( new TesteGamePad() );

        connect(this, &InterfaceJoystick::SIGNAL_disableGamePadJoystick,
                gamepad.get(), &GamePadJoystick::SLOT_disableGamePadJoystick);

        connect(this, &InterfaceJoystick::SIGNAL_enableGamePadJoystick,
                gamepad.get(), &GamePadJoystick::SLOT_enableGamePadJoystick);

        connect(gamepad.get(), &GamePadJoystick::SIGNAL_axisEvent,
                testeGamePad.get(), &TesteGamePad::SLOT_getAxisEventGamePad);

        connect(gamepad.get(), &GamePadJoystick::SIGNAL_buttonEvent,
                testeGamePad.get(), &TesteGamePad::SLOT_getButtonEventGamePad);

        connect(gamepad.get(), &GamePadJoystick::SIGNAL_buttonWithValueEvent,
                testeGamePad.get(), &TesteGamePad::SLOT_getButtonWithValueEventGamePad);

        connect(testeGamePad.get(), &TesteGamePad::SIGNAL_close,
                this, &InterfaceJoystick::SLOT_finalizaTesteComOGamepad);


        emit SIGNAL_enableGamePadJoystick();

        testeGamePad->setGamepadName(gamepad->sGetName());
        testeGamePad->show();

    }
}

void InterfaceJoystick::SLOT_finalizaTesteComOGamepad()
{
    testeGamePad.reset();
}

qint8 InterfaceJoystick::iConverteEscalaAxis_100_100(qint16 iAxis)
{
    return static_cast<qint8>( iAxis/327.67 );
}

qint8 InterfaceJoystick::iConverteEscalaAxis_0_100(qint16 iAxis)
{
    return static_cast<qint8>( (iAxis + axisMaximumV)/655.34 );
}

qint8 InterfaceJoystick::iConverteEscalaButtonWithValue_0_100(qint16 iAxis)
{
    return static_cast<qint8>( iAxis/327.67 );
}

QVector4D InterfaceJoystick::vt4dConverteLinearRotacao(QVector2D vt2dVel, float fVw)
{
    Eigen::MatrixXf W(4,1);
    Eigen::MatrixXf MR(4,3);
    Eigen::MatrixXf V(3,1);
    const float teta = 33*M_PI/180, wheelDistance = 80.5e-3, wheelRadius = 27e-3;

    W << 0,
         0,
         0,
         0;

    MR << -sin(teta),  cos(teta), wheelDistance,
           sin(teta),  cos(teta), wheelDistance,
           sin(teta), -cos(teta), wheelDistance,
          -sin(teta), -cos(teta), wheelDistance;

    V << vt2dVel.x() * 2.5 / 100,
         vt2dVel.y() * 2.5 / 100,
         fVw * 6.5 / 100;

    W = MR * V / wheelRadius;
    W = W * 60 / (2 * M_PI);

    // double max = qMax(qMax(qAbs(W(3)),qAbs(W(2))), qMax(qAbs(W(1)),qAbs(W(0))));
    // if(max > 127)
    // {
    //     double ajuste = max/127;
    //     W(3) /= ajuste;
    //     W(2) /= ajuste;
    //     W(1) /= ajuste;
    //     W(0) /= ajuste;
    // }

    return QVector4D(W(3), W(2), W(1), W(0));
}

void InterfaceJoystick::vReiniciaGrafico()
{
    vt_time_graph.clear();
    vt_time_graph.reserve(iControle);

    for(int i=0; i<iControle; i++)
    {
        vt_time_graph.append(i);
    }

    vel_roda_0_r = QVector<double>(iControle, 0);
    vel_roda_1_r = QVector<double>(iControle, 0);
    vel_roda_2_r = QVector<double>(iControle, 0);
    vel_roda_3_r = QVector<double>(iControle, 0);

    vel_roda_0_t = QVector<double>(iControle, 0);
    vel_roda_1_t = QVector<double>(iControle, 0);
    vel_roda_2_t = QVector<double>(iControle, 0);
    vel_roda_3_t = QVector<double>(iControle, 0);

    i_time_graph = 0;


    ui->cp_Odometria->graph(0)->setData(vt_time_graph, vel_roda_0_r);
    ui->cp_Odometria->graph(1)->setData(vt_time_graph, vel_roda_1_r);
    ui->cp_Odometria->graph(2)->setData(vt_time_graph, vel_roda_2_r);
    ui->cp_Odometria->graph(3)->setData(vt_time_graph, vel_roda_3_r);

    ui->cp_Odometria->graph(4)->setData(vt_time_graph, vel_roda_0_t);
    ui->cp_Odometria->graph(5)->setData(vt_time_graph, vel_roda_1_t);
    ui->cp_Odometria->graph(6)->setData(vt_time_graph, vel_roda_2_t);
    ui->cp_Odometria->graph(7)->setData(vt_time_graph, vel_roda_3_t);

    ui->cp_Odometria->replot();

}

void InterfaceJoystick::vIniciaGrafico()
{
    ui->cp_Odometria->legend->setVisible(true);
    ui->cp_Odometria->legend->setBrush(QColor("#505050"));
    ui->cp_Odometria->legend->setBorderPen(QPen(Qt::white));
    ui->cp_Odometria->legend->setTextColor(Qt::white);

    vt_time_graph.clear();
    vt_time_graph.reserve(iControle);

    for(int i=0; i<iControle; i++)
    {
        vt_time_graph.append(i);
    }

    vel_roda_0_r = QVector<double>(iControle, 0);
    vel_roda_1_r = QVector<double>(iControle, 0);
    vel_roda_2_r = QVector<double>(iControle, 0);
    vel_roda_3_r = QVector<double>(iControle, 0);

    vel_roda_0_t = QVector<double>(iControle, 0);
    vel_roda_1_t = QVector<double>(iControle, 0);
    vel_roda_2_t = QVector<double>(iControle, 0);
    vel_roda_3_t = QVector<double>(iControle, 0);

    i_time_graph = 0;

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(0)->setData(vt_time_graph, vel_roda_0_r);
    ui->cp_Odometria->graph(0)->setPen(QPen(Qt::yellow, 1)); /// amarelo
    ui->cp_Odometria->graph(0)->setName("Wheel 1 R");

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(1)->setData(vt_time_graph, vel_roda_1_r);
    ui->cp_Odometria->graph(1)->setPen(QPen(Qt::green, 1)); /// verde
    ui->cp_Odometria->graph(1)->setName("Wheel 2 R");

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(2)->setData(vt_time_graph, vel_roda_2_r);
    ui->cp_Odometria->graph(2)->setPen(QPen(Qt::cyan, 1)); /// azul
    ui->cp_Odometria->graph(2)->setName("Wheel 3 R");

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(3)->setData(vt_time_graph, vel_roda_3_r);
    ui->cp_Odometria->graph(3)->setPen(QPen(Qt::magenta, 1)); /// rosa
    ui->cp_Odometria->graph(3)->setName("Wheel 4 R");

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(4)->setData(vt_time_graph, vel_roda_0_t);
    ui->cp_Odometria->graph(4)->setPen(QPen(Qt::yellow, 1 , Qt::DashLine)); /// amarelo
    ui->cp_Odometria->graph(4)->setName("Wheel 1 T");

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(5)->setData(vt_time_graph, vel_roda_1_t);
    ui->cp_Odometria->graph(5)->setPen(QPen(Qt::green, 1,  Qt::DashLine)); /// verde
    ui->cp_Odometria->graph(5)->setName("Wheel 2 T");

    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(6)->setData(vt_time_graph, vel_roda_2_t);
    ui->cp_Odometria->graph(6)->setPen(QPen(Qt::cyan, 1, Qt::DashLine)); /// azul
    ui->cp_Odometria->graph(6)->setName("Wheel 3 T");
    ui->cp_Odometria->addGraph();
    ui->cp_Odometria->graph(7)->setData(vt_time_graph, vel_roda_3_t);
    ui->cp_Odometria->graph(7)->setPen(QPen(Qt::magenta, 1, Qt::DashLine)); /// rosa
    ui->cp_Odometria->graph(7)->setName("Wheel 4 T");


    ui->cp_Odometria->xAxis->setLabel("");
    ui->cp_Odometria->xAxis->setVisible(false);
    ui->cp_Odometria->yAxis->setLabel("Velocidade [RPM]");

    ui->cp_Odometria->xAxis->setRange(0, iControle);
    ui->cp_Odometria->yAxis->setRange(-Y_RANGE, Y_RANGE);

    ui->cp_Odometria->xAxis->setTickLabelColor(Qt::white);
    ui->cp_Odometria->yAxis->setTickLabelColor(Qt::white);

    ui->cp_Odometria->xAxis->setLabelColor(Qt::white);
    ui->cp_Odometria->yAxis->setLabelColor(Qt::white);

    ui->cp_Odometria->xAxis->setLabelColor(Qt::white);
    ui->cp_Odometria->yAxis->setLabelColor(Qt::white);

    ui->cp_Odometria->setBackground(QColor("#404040"));
    ui->cp_Odometria->xAxis->setTickPen(QPen(Qt::white));
    ui->cp_Odometria->xAxis->setBasePen(QPen(Qt::white));
    ui->cp_Odometria->xAxis->setSubTickPen(QPen(Qt::white));
    ui->cp_Odometria->xAxis->setTickLabelColor(Qt::white);

    ui->cp_Odometria->yAxis->setTickPen(QPen(Qt::white));
    ui->cp_Odometria->yAxis->setBasePen(QPen(Qt::white));
    ui->cp_Odometria->yAxis->setSubTickPen(QPen(Qt::white));
    ui->cp_Odometria->yAxis->setTickLabelColor(Qt::white);


    ui->cp_Odometria->replot();

}

void InterfaceJoystick::configureDarkStyle()
{
    qApp->setStyle (QStyleFactory::create ("Fusion"));
    QPalette darkPalette;
    darkPalette.setColor (QPalette::BrightText,      Qt::red);
    darkPalette.setColor (QPalette::WindowText,      Qt::white);
    darkPalette.setColor (QPalette::ToolTipBase,     Qt::white);
    darkPalette.setColor (QPalette::ToolTipText,     Qt::white);
    darkPalette.setColor (QPalette::Text,            Qt::white);
    darkPalette.setColor (QPalette::ButtonText,      Qt::white);
    darkPalette.setColor (QPalette::HighlightedText, Qt::black);
    darkPalette.setColor (QPalette::Window,          QColor (53, 53, 53));
    darkPalette.setColor (QPalette::Base,            QColor (25, 25, 25));
    darkPalette.setColor (QPalette::AlternateBase,   QColor (53, 53, 53));
    darkPalette.setColor (QPalette::Button,          QColor (53, 53, 53));
    darkPalette.setColor (QPalette::Link,            QColor (42, 130, 218));
    darkPalette.setColor (QPalette::Highlight,       QColor (42, 130, 218));
    qApp->setPalette (darkPalette);
}


void InterfaceJoystick::vSetBaterryLevelInterface(float baterryLevel){

    QString bat = QString::number(static_cast<double>(baterryLevel),'f', 2);
    ui->batteryValueLabel->setText(bat);

    if (baterryLevel>11.f)
        ui->batteryValueLabel->setStyleSheet("QLabel { background-color : green; color : white; }");
    else
        ui->batteryValueLabel->setStyleSheet("QLabel { background-color : red; color : white; }");
}

void InterfaceJoystick::vSetKickSensorInterface(bool kickSensor){

    if(kickSensor)
        ui->kickSensorValue->setStyleSheet("QLabel { background-color : green; color : white; }");
    else
        ui->kickSensorValue->setStyleSheet("QLabel { background-color : red; color : white; }");
}


void InterfaceJoystick:: vSetRollerOnOffInterface (bool b){

    if(b)
        ui->l_Roller_ON_OFF->setStyleSheet("QLabel { background-color : green; color : white; }");
    else
        ui->l_Roller_ON_OFF->setStyleSheet("QLabel { background-color : red; color : white; }");

    robot->roller = b;
}
void InterfaceJoystick::vSetKICK_TYPE_Interface (KICKTYPE type){

    switch (type){
        case KICK_NONE:
            ui->l_KICK_TYPE->setText("KICK_NONE");
            break;
        case KICK_CUSTOM:
            ui->l_KICK_TYPE->setText("KICK_CUSTOM");
            break;
        case CHIP_KICK_CUSTOM:
            ui->l_KICK_TYPE->setText("CHIP_KICK_CUSTOM");
            break;
        default:
            break;
    }
}


void InterfaceJoystick::vUpdateGraficoInterface(int dRoda_0_r,
                                                int dRoda_1_r,
                                                int dRoda_2_r,
                                                int dRoda_3_r,
                                                int dRoda_0_t,
                                                int dRoda_1_t,
                                                int dRoda_2_t,
                                                int dRoda_3_t)
{
    if(iContadorVelocidadeGrafico >= iFatorVelocidadeGrafico)
    {
        vel_roda_0_r.replace(i_time_graph, dRoda_0_r);
        vel_roda_1_r.replace(i_time_graph, dRoda_1_r);
        vel_roda_2_r.replace(i_time_graph, dRoda_2_r);
        vel_roda_3_r.replace(i_time_graph, dRoda_3_r);

        vel_roda_0_t.replace(i_time_graph, dRoda_0_t);
        vel_roda_1_t.replace(i_time_graph, dRoda_1_t);
        vel_roda_2_t.replace(i_time_graph, dRoda_2_t);
        vel_roda_3_t.replace(i_time_graph, dRoda_3_t);


        ui->cp_Odometria->graph(0)->setData(vt_time_graph, vel_roda_0_r);
        ui->cp_Odometria->graph(1)->setData(vt_time_graph, vel_roda_1_r);
        ui->cp_Odometria->graph(2)->setData(vt_time_graph, vel_roda_2_r);
        ui->cp_Odometria->graph(3)->setData(vt_time_graph, vel_roda_3_r);

        ui->cp_Odometria->graph(4)->setData(vt_time_graph, vel_roda_0_t);
        ui->cp_Odometria->graph(5)->setData(vt_time_graph, vel_roda_1_t);
        ui->cp_Odometria->graph(6)->setData(vt_time_graph, vel_roda_2_t);
        ui->cp_Odometria->graph(7)->setData(vt_time_graph, vel_roda_3_t);

        ui->cp_Odometria->replot();

        iContadorVelocidadeGrafico = 0;


        ++i_time_graph;

        if(i_time_graph == iControle)
        {
            i_time_graph = 0;
        }
    }
    else
    {
        iContadorVelocidadeGrafico++;
    }
}

bool InterfaceJoystick::bConectouRadio()
{
    Settings conf;

    conf.baudRate = static_cast<QSerialPort::BaudRate>
            (ui->baudRateBox->itemData(ui->baudRateBox->currentIndex()).toInt());

    conf.name = ui->portNameBox->currentText();

    conf.dataBits=QSerialPort::DataBits::Data8;
    conf.flowControl=QSerialPort::FlowControl::NoFlowControl;
    conf.parity=QSerialPort::Parity::NoParity;
    conf.stopBits=QSerialPort::StopBits::OneStop;

    conf.localEchoEnabled= ui->localEchoEnabledCheck->isChecked();

    radio->setConfig(&conf);



    if(radio->configureSerialPort()){

        qInfo("Rádio conectado");

        timerUpdateRadio->start(10);

        return true;
    }
    else {
        qInfo("Rádio não conectado");
        qWarning()<<"Open Error:" + radio->serial->errorString();
        return false;
    }

    return false;
}

void InterfaceJoystick::vDesconectarRadio()
{
    radio->closePort();

    qInfo("Rádio desconectado");

    if(timerUpdateRadio->isActive())
        timerUpdateRadio->stop();

    bRadioConectado = false;
}

void InterfaceJoystick::on_actionAbout_triggered()
{
    QMessageBox::about(this, "Joystick Robot Controller - RoboFEI / Small Size League",
                       "Joystick Robot Controller (v" + QString(PROJECT_VERSION) + ")\n"
                       "RoboFEI / Small Size League \n" +
                       "Copyright (C) 2020-2022  RoboFEI / Small Size League\n"
                       "\n"
                       "This program is free software: you can redistribute it and/or modify"
                       " it under the terms of the GNU General Public License as published by"
                       " the Free Software Foundation, either version 3 of the License, or"
                       " (at your option) any later version.\n"
                       "\n"
                       "This program is distributed in the hope that it will be useful,"
                       " but WITHOUT ANY WARRANTY; without even the implied warranty of"
                       " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
                       " GNU General Public License for more details.\n"
                       "\n"
                       "You should have received a copy of the GNU General Public License"
                       " along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
                       "\n"
                       "Source code available here:\n" +
                       "<https://gitlab.com/robofei/ssl/joystickrobotcontroller>"
                       );
}

void InterfaceJoystick::on_actionGitLab_Main_Page_triggered()
{
    QDesktopServices::openUrl(QUrl("https://gitlab.com/robofei/ssl/joystickrobotcontroller"));
}

void InterfaceJoystick::on_cb_velocityViewer_stateChanged(int arg1)
{
    if(joystickAtual != JoystickAtual::NONE)
        ui->velocityViewer->setVisible(arg1 == Qt::Checked);
}

void InterfaceJoystick::on_cb_odometryLegend_stateChanged(int arg1)
{
    ui->cp_Odometria->legend->setVisible(arg1 == Qt::Checked);

    if(ui->cp_Odometria->isVisible())
    {
        ui->cp_Odometria->replot();
    }
}

void InterfaceJoystick::on_cb_odometry_stateChanged(int arg1)
{
    if(joystickAtual != JoystickAtual::NONE)
        ui->cp_Odometria->setVisible(arg1 == Qt::Checked);
}

void InterfaceJoystick::on_cb_odometryWheel0_stateChanged(int arg1)
{
    ui->cp_Odometria->graph(0)->setVisible(arg1 == Qt::Checked);
    // ui->cp_Odometria->graph(4)->setVisible(arg1 == Qt::Checked);

    if(ui->cp_Odometria->isVisible())
    {
        ui->cp_Odometria->replot();
    }
}

void InterfaceJoystick::on_cb_odometryWheel1_stateChanged(int arg1)
{
    ui->cp_Odometria->graph(1)->setVisible(arg1 == Qt::Checked);
    // ui->cp_Odometria->graph(5)->setVisible(arg1 == Qt::Checked);

    if(ui->cp_Odometria->isVisible())
    {
        ui->cp_Odometria->replot();
    }
}

void InterfaceJoystick::on_cb_odometryWheel2_stateChanged(int arg1)
{
    ui->cp_Odometria->graph(2)->setVisible(arg1 == Qt::Checked);
    // ui->cp_Odometria->graph(6)->setVisible(arg1 == Qt::Checked);

    if(ui->cp_Odometria->isVisible())
    {
        ui->cp_Odometria->replot();
    }
}

void InterfaceJoystick::on_cb_odometryWheel3_stateChanged(int arg1)
{
    ui->cp_Odometria->graph(3)->setVisible(arg1 == Qt::Checked);
    // ui->cp_Odometria->graph(7)->setVisible(arg1 == Qt::Checked);

    if(ui->cp_Odometria->isVisible())
    {
        ui->cp_Odometria->replot();
    }
}


void InterfaceJoystick::on_sb_Vx_valueChanged(int arg1)
{
    ui->pB_Vx->setValue(arg1);
}


void InterfaceJoystick::on_sb_Vy_valueChanged(int arg1)
{
    ui->pB_Vy->setValue(arg1);
}


void InterfaceJoystick::on_spinBox_3_valueChanged(int arg1)
{
    ui->pB_W->setValue(arg1);
}

