#############################################################################
# Joystick Robot Controller                                                 #
# Copyright (C) 2021-2022  Bruno Bollos Correa                              #
# Copyright (C) 2021-2022  RoboFEI / Small Size League                      #
#                                                                           #
# This program is free software: you can redistribute it and/or modify      #
# it under the terms of the GNU General Public License as published by      #
# the Free Software Foundation, either version 3 of the License, or         #
# (at your option) any later version.                                       #
#                                                                           #
# This program is distributed in the hope that it will be useful,           #
# but WITHOUT ANY WARRANTY; without even the implied warranty of            #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
# GNU General Public License for more details.                              #
#                                                                           #
# You should have received a copy of the GNU General Public License         #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#############################################################################


TEMPLATE = app
TARGET = "JoystickRobotController_RoboFEI-SSL"

QT += \
    widgets \
    core \
    gui \
    serialport \
    gamepad

greaterThan(QT_MAJOR_VERSION, 4)

include (include/QCustomPlot/QCustomPlot.pri)

win32 {
    RC_ICONS = share/icon/joystickrobotcontroller_icon.ico
}

FORMS += \
    src/Interface/interfacejoystick.ui \
    src/TesteGamePad/testegamepad.ui

HEADERS += \
    src/GamePadJoystick/gamepadjoystick.h \
    src/Interface/interfacejoystick.h \
    src/Radio/radiobase.h \
    src/Robot/robot.h \
    src/VelocityViewer/velocityviewerwidget.h \
    src/Version/version.h \
    src/VirtualJoystick/virtualjoystick.h  \
    src/TesteGamePad/testegamepad.h

SOURCES += \
    src/VelocityViewer/velocityviewerwidget.cpp \
    src/main.cpp \
    src/GamePadJoystick/gamepadjoystick.cpp \
    src/Interface/interfacejoystick.cpp \
    src/Radio/radiobase.cpp \
    src/VirtualJoystick/virtualjoystick.cpp  \
    src/TesteGamePad/testegamepad.cpp

INCLUDEPATH +=  \
    src/ \
    include/

INCLUDEPATH += /usr/include/eigen3
DEPENDPATH += /usr/include/eigen3

RESOURCES += \
    share/buttons/KeyboardButtons.qrc \
    share/buttons/XBoxButtons.qrc \
    share/joystickrobotcontroller_icon.qrc
