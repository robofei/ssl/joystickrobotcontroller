[**Leia-me em portugês**](https://gitlab.com/robofei/ssl/joystickrobotcontroller/-/blob/master/README_pt-br.md)


# Installation

[**Stable releases available here**](https://gitlab.com/robofei/ssl/joystickrobotcontroller/-/releases)

## Installing Dependencies

On Ubuntu and Ubuntu-based distributions, install the dependencies, with `apt-get`, with the command below:

```sh
sudo apt install libeigen3-dev qtbase5-dev qt5-qmake libqt5serialport5-dev libqt5gamepad5-dev
```

For Arch Linux and Manjaro, install the dependencies with `pacman`:
```sh
sudo pacman -S eigen qt5-base qt5-serialport qt5-gamepad
```

On Windows, download the latest version of QtCreator [here](https://download.qt.io/archive/online_installers/) and follow the instructions to install it.

Note: Install Qt5, not Qt6.

## Building and running the program

### GNU/Linux

First of all, clone the repository and enter into it:
```sh
git clone https://gitlab.com/robofei/ssl/joystickrobotcontroller.git
cd joystickrobotcontroller
```

Then use `qmake` to create the Makefile:
```sh
mkdir build && cd build
qmake .. CONFIG+=release
```

Then run `make` to start the building process:
```sh
make
```

The executable `JoystickRobotController_RoboFEI-SSL` should be generated in the working directory. To run it, type:
```sh
./JoystickRobotController_RoboFEI-SSL
```

### Windows

Open QtCreator, go to **File -> New File or Project -> Projects -> Import Projects -> git Clone** and then click on **Choose**. Fill in the _Repository_ field with the repository link, the rest in any way you like and proceed.

Download the latest stable version of eigen3 [from this site](https://gitlab.com/libeigen/eigen/-/releases) by clicking on **Source Code (zip)**. Extract the file into `.../project_directory/include` and rename the `eigen-3.X.Y` directory to just `eigen3`.

Go back to QtCreator and click on the _run_ button (lower left corner) to build and compile the program.


## Cross Compiling with MXE

`export PATH="$PATH:$HOME/Repositorios/mxe/usr/bin/"` 
