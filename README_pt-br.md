
# Instalação

[**Releases estáveis disponíveis aqui**](https://gitlab.com/robofei/ssl/joystickrobotcontroller/-/releases)

## Instalando dependências

No Ubuntu ou em distribuições baseadas em Ubuntu, instale as dependências do software, com `apt-get`, com o comando abaixo:

```sh
sudo apt install libeigen3-dev qtbase5-dev qt5-qmake libqt5serialport5-dev libqt5gamepad5-dev
```

Para Arch Linux e Manjaro, instale as dependências com `pacman`:
```sh
sudo pacman -S eigen qt5-base qt5-serialport qt5-gamepad
```

No Windows, baixe a versão mais recente do QtCreator [aqui](https://download.qt.io/archive/online_installers/) e siga as instruções para instalação do programa.

Nota: Instale Qt5, não Qt6.

## Compilando e rodando o programa

### GNU/Linux

Antes de tudo, clone o repositório e adentre nele:
```sh
git clone https://gitlab.com/robofei/ssl/joystickrobotcontroller.git
cd joystickrobotcontroller
```

Depois use `qmake` para criar a Makefile:
```sh
mkdir build && cd build
qmake .. CONFIG+=release
```

Em seguida, execute `make` para iniciar a compilação:
```sh
make
```

O executável `JoystickRobotController_RoboFEI-SSL` deve ser gerado no diretório de trabalho. Para executá-lo, digite:
```sh
./JoystickRobotController_RoboFEI-SSL
```

### Windows

Abra o QtCreator, vá em **File -> New File or Project -> Projects -> Import Projects -> git Clone** e, em seguida, clique em **Choose**. Preencha o campo _Repository_ com o link do repositório e o restante da maneira que preferir e prossiga.

Baixe a versão estável mais recente de eigen3 [nesse site](https://gitlab.com/libeigen/eigen/-/releases) clicando em **Source Code (zip)**. Extraia o arquivo em `.../diretório_projeto/include` e renomeie o nome do diretório `eigen-3.X.Y` para apenas `eigen3`.

Volte no QtCreator e clique no botão _run_ (ponta inferior esquerda) para buildar e compilar o programa.
